<?php
namespace Jtl\Fulfillment\Api\Sdk\Models;

use Izzle\Model\Model;

/**
 * Class DataModel
 * @package Jtl\Fulfillment\Api\Sdk\Models
 */
abstract class DataModel extends Model
{
    /**
     * @var array - Properties that are not touched during serialization
     */
    protected $preservables = [];

    /**
     * @return array
     */
    public function getPreservables(): array
    {
        return $this->preservables;
    }

    /**
     * @param array $preservables
     * @return DataModel
     */
    public function setPreservables(array $preservables): DataModel
    {
        $this->preservables = $preservables;
        return $this;
    }

    /**
     * @param string $preservable
     * @return bool
     */
    public function hasPreservable(string $preservable): bool
    {
        return in_array($preservable, $this->preservables, true);
    }
}
