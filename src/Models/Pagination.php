<?php
namespace Jtl\Fulfillment\Api\Sdk\Models;

use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;

/**
 * Class Pagination
 * @package Jtl\Fulfillment\Api\Sdk\Models
 */
class Pagination extends DataModel
{
    /**
     * @var Model[]
     */
    protected $items = [];
    
    /**
     * @var Page|null
     */
    protected $page;
    
    /**
     * @var Link|null
     */
    protected $links;
    
    /**
     * @return Model[]
     */
    public function getItems(): array
    {
        return $this->items;
    }
    
    /**
     * @param Model[] $items
     * @return Pagination
     */
    public function setItems(array $items): Pagination
    {
        $this->items = $items;
        
        return $this;
    }
    
    /**
     * @param Model $item
     * @param string|null $key
     * @return Pagination
     */
    public function addItem(Model $item, ?string $key = null): Pagination
    {
        if ($key === null) {
            $this->items[] = $item;
        } else {
            $this->items[$key] = $item;
        }
        
        return $this;
    }
    
    /**
     * @return Page|null
     */
    public function getPage(): ?Page
    {
        return $this->page;
    }
    
    /**
     * @param Page $page
     * @return Pagination
     */
    public function setPage(Page $page): Pagination
    {
        $this->page = $page;
        
        return $this;
    }
    
    /**
     * @return Link|null
     */
    public function getLinks(): ?Link
    {
        return $this->links;
    }
    
    /**
     * @param Link $links
     * @return Pagination
     */
    public function setLinks(Link $links): Pagination
    {
        $this->links = $links;
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return (new PropertyCollection())->setProperties([
            new PropertyInfo('items', 'mixed', []),
            new PropertyInfo('page', Page::class, null, true),
            new PropertyInfo('links', Link::class, null, true)
        ]);
    }
}
