<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Outbound;

use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\General\Outbound\OutboundShippingNotification as GeneralOutboundShippingNotification;

/**
 * Class OutboundShippingNotification
 * @package Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Outbound
 */
class OutboundShippingNotification extends GeneralOutboundShippingNotification
{
    /**
     * @var string|null
     */
    protected $merchantId;
    
    /**
     * @return string|null
     */
    public function getMerchantId(): ?string
    {
        return $this->merchantId;
    }
    
    /**
     * @param string|null $merchantId
     * @return OutboundShippingNotification
     */
    public function setMerchantId(?string $merchantId): OutboundShippingNotification
    {
        $this->merchantId = $merchantId;
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return parent::loadProperties()->addProperties([
            new PropertyInfo('merchantId', 'string', null),
        ]);
    }
}
