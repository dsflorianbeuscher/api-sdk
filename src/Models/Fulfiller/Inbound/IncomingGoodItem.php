<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Inbound;

use DateTime;
use DateTimeZone;
use Exception;
use function is_string;
use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\General\BestBefore;

/**
 * Class IncomingGoodItem
 * @package Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Inbound
 */
class IncomingGoodItem extends DataModel
{
    /**
     * @var string|null
     */
    protected $jfsku;
    
    /**
     * @var string|null
     */
    protected $warehouseId;
    
    /**
     * @var float|null
     */
    protected $quantityBlocked;
    
    /**
     * @var float|null
     */
    protected $quantity;
    
    /**
     * @var string|null
     */
    protected $note;
    
    /**
     * @var string|null
     */
    protected $batch;
    
    /**
     * @var BestBefore|null
     */
    protected $bestBefore;
    
    /**
     * @var DateTime|null
     */
    protected $fulfillerTimestamp;
    
    /**
     * @var string
     */
    protected $fulfillerStockChangeId;
    
    /**
     * @return string|null
     */
    public function getJfsku(): ?string
    {
        return $this->jfsku;
    }
    
    /**
     * @param string|null $jfsku
     * @return IncomingGoodItem
     */
    public function setJfsku(?string $jfsku): IncomingGoodItem
    {
        $this->jfsku = $jfsku;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getWarehouseId(): ?string
    {
        return $this->warehouseId;
    }
    
    /**
     * @param string|null $warehouseId
     * @return IncomingGoodItem
     */
    public function setWarehouseId(?string $warehouseId): IncomingGoodItem
    {
        $this->warehouseId = $warehouseId;
        
        return $this;
    }
    
    /**
     * @return float|null
     */
    public function getQuantityBlocked(): ?float
    {
        return $this->quantityBlocked;
    }
    
    /**
     * @param float|null $quantityBlocked
     * @return IncomingGoodItem
     */
    public function setQuantityBlocked(?float $quantityBlocked): IncomingGoodItem
    {
        $this->quantityBlocked = $quantityBlocked;
        
        return $this;
    }
    
    /**
     * @return float|null
     */
    public function getQuantity(): ?float
    {
        return $this->quantity;
    }
    
    /**
     * @param float|null $quantity
     * @return IncomingGoodItem
     */
    public function setQuantity(?float $quantity): IncomingGoodItem
    {
        $this->quantity = $quantity;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getNote(): ?string
    {
        return $this->note;
    }
    
    /**
     * @param string|null $note
     * @return IncomingGoodItem
     */
    public function setNote(?string $note): IncomingGoodItem
    {
        $this->note = $note;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getBatch(): ?string
    {
        return $this->batch;
    }
    
    /**
     * @param string|null $batch
     * @return IncomingGoodItem
     */
    public function setBatch(?string $batch): IncomingGoodItem
    {
        $this->batch = $batch;
        
        return $this;
    }
    
    /**
     * @return BestBefore|null
     */
    public function getBestBefore(): ?BestBefore
    {
        return $this->bestBefore;
    }
    
    /**
     * @param BestBefore $bestBefore
     * @return IncomingGoodItem
     */
    public function setBestBefore(BestBefore $bestBefore): IncomingGoodItem
    {
        $this->bestBefore = $bestBefore;
        
        return $this;
    }
    
    /**
     * @return DateTime|null
     */
    public function getFulfillerTimestamp(): ?DateTime
    {
        return $this->fulfillerTimestamp;
    }
    
    /**
     * @param DateTime|string|null $fulfillerTimestamp
     * @return IncomingGoodItem
     * @throws Exception
     */
    public function setFulfillerTimestamp($fulfillerTimestamp): IncomingGoodItem
    {
        if ($fulfillerTimestamp === null) {
            return $this;
        }
    
        if (is_string($fulfillerTimestamp)) {
            $fulfillerTimestamp = (new DateTime($fulfillerTimestamp))->setTimezone(new DateTimeZone('UTC'));
        }
    
        $this->checkDate($fulfillerTimestamp);
        $this->fulfillerTimestamp = $fulfillerTimestamp;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getFulfillerStockChangeId(): ?string
    {
        return $this->fulfillerStockChangeId;
    }
    
    /**
     * @param string|null $fulfillerStockChangeId
     * @return IncomingGoodItem
     */
    public function setFulfillerStockChangeId(?string $fulfillerStockChangeId): IncomingGoodItem
    {
        $this->fulfillerStockChangeId = $fulfillerStockChangeId;
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('jfsku', 'string', null),
            new PropertyInfo('warehouseId', 'string', null),
            new PropertyInfo('quantityBlocked', 'float', null),
            new PropertyInfo('quantity', 'float', null),
            new PropertyInfo('note', 'string', null),
            new PropertyInfo('batch', 'string', null),
            new PropertyInfo('bestBefore', BestBefore::class, null, true),
            new PropertyInfo('fulfillerTimestamp', DateTime::class, null),
            new PropertyInfo('fulfillerStockChangeId', 'string', null)
        ]);
    }
}
