<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Stock;

use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\General\Stock\StockInboundItem as GeneralStockInboundItem;

/**
 * Class StockInboundItem
 * @package Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Stock
 */
class StockInboundItem extends GeneralStockInboundItem
{
    /**
     * @var string|null
     */
    protected $inboundId;
    
    /**
     * @return string|null
     */
    public function getInboundId(): ?string
    {
        return $this->inboundId;
    }
    
    /**
     * @param string|null $inboundId
     * @return StockInboundItem
     */
    public function setInboundId(?string $inboundId): StockInboundItem
    {
        $this->inboundId = $inboundId;
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return parent::loadProperties()->addProperty(new PropertyInfo('inboundId', 'string', null));
    }
}
