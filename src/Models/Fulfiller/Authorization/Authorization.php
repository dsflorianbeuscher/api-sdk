<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Authorization;

use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\General\ModificationInfo;

/**
 * Class Authorization
 * @package Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Authorization
 */
class Authorization extends DataModel
{
    /**
     * @var string|null
     */
    protected $merchantId;
    
    /**
     * @var AuthorizationWarehouse[]
     */
    protected $warehouses = [];
    
    /**
     * @var ModificationInfo|null
     */
    protected $modificationInfo;
    
    /**
     * @return string|null
     */
    public function getMerchantId(): ?string
    {
        return $this->merchantId;
    }
    
    /**
     * @param string|null $merchantId
     * @return Authorization
     */
    public function setMerchantId(?string $merchantId): Authorization
    {
        $this->merchantId = $merchantId;
        
        return $this;
    }
    
    /**
     * @return AuthorizationWarehouse[]
     */
    public function getWarehouses(): array
    {
        return $this->warehouses;
    }
    
    /**
     * @param AuthorizationWarehouse[] $warehouses
     * @return Authorization
     */
    public function setWarehouses(array $warehouses): Authorization
    {
        $this->warehouses = $warehouses;
        
        return $this;
    }
    
    /**
     * @param AuthorizationWarehouse $warehouse
     * @param string|null $key
     * @return Authorization
     */
    public function addWarehouse(AuthorizationWarehouse $warehouse, string $key = null): Authorization
    {
        if ($key === null) {
            $this->warehouses[] = $warehouse;
        } else {
            $this->warehouses[$key] = $warehouse;
        }
        
        return $this;
    }
    
    /**
     * @return ModificationInfo|null
     */
    public function getModificationInfo(): ?ModificationInfo
    {
        return $this->modificationInfo;
    }
    
    /**
     * @param ModificationInfo|null $modificationInfo
     * @return Authorization
     */
    public function setModificationInfo(?ModificationInfo $modificationInfo): Authorization
    {
        $this->modificationInfo = $modificationInfo;
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return (new PropertyCollection())->setProperties([
            new PropertyInfo('merchantId', 'string', null),
            new PropertyInfo('warehouses', AuthorizationWarehouse::class, null, true, true),
            new PropertyInfo('modificationInfo', ModificationInfo::class, null, true)
        ]);
    }
}
