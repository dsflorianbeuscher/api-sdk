<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Product;

use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Inbound\Inbound;
use Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Outbound\Outbound;
use Jtl\Fulfillment\Api\Sdk\Models\General\Product\Product as GeneralProduct;

/**
 * Class Product
 * @package Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Product
 */
class Product extends GeneralProduct
{
    /**
     * @var string|null
     */
    protected $merchantId;
    
    /**
     * @var Inbound[]
     */
    protected $openInbounds = [];
    
    /**
     * @var Outbound[]
     */
    protected $openOutbounds = [];
    
    /**
     * @return string|null
     */
    public function getMerchantId(): ?string
    {
        return $this->merchantId;
    }
    
    /**
     * @param string|null $merchantId
     * @return Product
     */
    public function setMerchantId(?string $merchantId): Product
    {
        $this->merchantId = $merchantId;
        
        return $this;
    }
    
    /**
     * @return Inbound[]
     */
    public function getOpenInbounds(): array
    {
        return $this->openInbounds;
    }
    
    /**
     * @param Inbound[] $openInbounds
     * @return Product
     */
    public function setOpenInbounds(array $openInbounds): Product
    {
        $this->openInbounds = $openInbounds;
        
        return $this;
    }
    
    /**
     * @param Inbound $inbound
     * @param string|null $key
     * @return Product
     */
    public function addOpenInbound(Inbound $inbound, ?string $key = null): Product
    {
        if ($key === null) {
            $this->openInbounds[] = $inbound;
        } else {
            $this->openInbounds[$key] = $inbound;
        }
        
        return $this;
    }
    
    /**
     * @return Outbound[]
     */
    public function getOpenOutbounds(): array
    {
        return $this->openOutbounds;
    }
    
    /**
     * @param Outbound[] $openOutbounds
     * @return Product
     */
    public function setOpenOutbounds(array $openOutbounds): Product
    {
        $this->openOutbounds = $openOutbounds;
        
        return $this;
    }
    
    /**
     * @param Outbound $outbound
     * @param string|null $key
     * @return Product
     */
    public function addOpenOutbound(Outbound $outbound, ?string $key = null): Product
    {
        if ($key === null) {
            $this->openOutbounds[] = $outbound;
        } else {
            $this->openOutbounds[$key] = $outbound;
        }
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return parent::loadProperties()->addProperties([
            new PropertyInfo('merchantId', 'string', null),
            new PropertyInfo('openInbounds', Inbound::class, [], true, true),
            new PropertyInfo('openOutbounds', Outbound::class, [], true, true)
        ]);
    }
}
