<?php
namespace Jtl\Fulfillment\Api\Sdk\Models;

use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;

/**
 * Class Page
 * @package Jtl\Fulfillment\Api\Sdk\Models
 */
class Page extends DataModel
{
    /**
     * @var int
     */
    protected $total = 0;
    
    /**
     * @var int
     */
    protected $limit = 100;
    
    /**
     * @var int
     */
    protected $offset = 0;
    
    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }
    
    /**
     * @param int $total
     * @return Page
     */
    public function setTotal(int $total): Page
    {
        $this->total = $total;
        
        return $this;
    }
    
    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }
    
    /**
     * @param int $limit
     * @return Page
     */
    public function setLimit(int $limit): Page
    {
        $this->limit = $limit;
        
        return $this;
    }
    
    /**
     * @return int
     */
    public function getOffset(): int
    {
        return $this->offset;
    }
    
    /**
     * @param int $offset
     * @return Page
     */
    public function setOffset(int $offset): Page
    {
        $this->offset = $offset;
        
        return $this;
    }
    
    /**
     * @param int $page
     * @return Page
     */
    public function setCurrentPage(int $page): Page
    {
        $this->offset = ($page - 1) * $this->limit;
        
        return $this;
    }
    
    /**
     * @return int
     */
    public function getLastPage(): int
    {
        return $this->limit > 0 ? max((int) ceil($this->total / $this->limit), 1) : 1;
    }
    
    /**
     * @return int
     */
    public function getCurrentPage(): int
    {
        return $this->limit > 0 ? $this->offset / $this->limit + 1 : 1;
    }
    
    /**
     * @return int|null
     */
    public function getFrom(): ?int
    {
        return $this->getFirstItem();
    }
    
    /**
     * @return int|null
     */
    public function getTo(): ?int
    {
        return $this->getLastItem();
    }
    
    /**
     * @return bool
     */
    public function hasPages(): bool
    {
        return $this->getCurrentPage() !== 1 || $this->hasMorePages();
    }
    
    /**
     * @return bool
     */
    public function onFirstPage(): bool
    {
        return $this->getCurrentPage() <= 1;
    }
    
    /**
     * @return bool
     */
    public function hasMorePages(): bool
    {
        return $this->getCurrentPage() < $this->getLastPage();
    }
    
    public function countOnPage(): int
    {
        if ($this->hasMorePages()) {
            return $this->limit;
        }
        
        return $this->total - ($this->getCurrentPage() - 1) * $this->limit;
    }
    
    /**
     * @return int|null
     */
    protected function getFirstItem(): ?int
    {
        return $this->total > 0 ? ($this->getCurrentPage() - 1) * $this->limit + 1 : null;
    }
    
    /**
     * @return int|null
     */
    protected function getLastItem(): ?int
    {
        return $this->total > 0 ? $this->getFirstItem() + $this->countOnPage() - 1 : null;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return (new PropertyCollection())->setProperties([
            new PropertyInfo('total', 'int', 0),
            new PropertyInfo('limit', 'int', 100),
            new PropertyInfo('offset', 'int', 0),
            new PropertyInfo('lastPage', 'int', 1),
            new PropertyInfo('currentPage', 'int', 1),
            new PropertyInfo('from', 'int', null),
            new PropertyInfo('to', 'int', null)
        ]);
    }
}
