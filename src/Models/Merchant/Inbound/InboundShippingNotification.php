<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\Merchant\Inbound;

use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\General\Inbound\InboundShippingNotification as GeneralInboundShippingNotification;

class InboundShippingNotification extends GeneralInboundShippingNotification
{
    /**
     * @var string|null
     */
    protected $fulfillerId;
    
    /**
     * @var string|null
     */
    protected $merchantInboundNumber;
    
    /**
     * @return string|null
     */
    public function getFulfillerId(): ?string
    {
        return $this->fulfillerId;
    }
    
    /**
     * @param string|null $fulfillerId
     * @return InboundShippingNotification
     */
    public function setFulfillerId(?string $fulfillerId): InboundShippingNotification
    {
        $this->fulfillerId = $fulfillerId;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getMerchantInboundNumber(): ?string
    {
        return $this->merchantInboundNumber;
    }
    
    /**
     * @param string|null $merchantInboundNumber
     * @return InboundShippingNotification
     */
    public function setMerchantInboundNumber(?string $merchantInboundNumber): InboundShippingNotification
    {
        $this->merchantInboundNumber = $merchantInboundNumber;
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return parent::loadProperties()->addProperties([
            new PropertyInfo('fulfillerId', 'string', null),
            new PropertyInfo('merchantInboundNumber', 'string', null)
        ]);
    }
}
