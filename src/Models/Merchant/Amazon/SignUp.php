<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\Merchant\Amazon;

use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;

class SignUp extends DataModel
{
    /**
     * @var string|null
     */
    protected $redirectUrl;

    /**
     * @var int|null
     */
    protected $expiresIn;

    /**
     * @return string|null
     */
    public function getRedirectUrl(): ?string
    {
        return $this->redirectUrl;
    }

    /**
     * @param string|null $redirectUrl
     * @return SignUp
     */
    public function setRedirectUrl(?string $redirectUrl): SignUp
    {
        $this->redirectUrl = $redirectUrl;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getExpiresIn(): ?int
    {
        return $this->expiresIn;
    }

    /**
     * @param int|null $expiresIn
     * @return SignUp
     */
    public function setExpiresIn(?int $expiresIn): SignUp
    {
        $this->expiresIn = $expiresIn;
        return $this;
    }

    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('redirectUrl', 'string', null),
            new PropertyInfo('expiresIn', 'int', null)
        ]);
    }
}
