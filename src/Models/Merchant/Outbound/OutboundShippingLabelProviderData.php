<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\Merchant\Outbound;

use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\DataModel;

class OutboundShippingLabelProviderData extends DataModel
{
    /**
     * @var OutboundAmazonSfpShippingLabelProviderData|null
     */
    protected $amazonSfpShippingLabelProviderData;
    
    /**
     * @return OutboundAmazonSfpShippingLabelProviderData|null
     */
    public function getAmazonSfpShippingLabelProviderData(): ?OutboundAmazonSfpShippingLabelProviderData
    {
        return $this->amazonSfpShippingLabelProviderData;
    }
    
    /**
     * @param OutboundAmazonSfpShippingLabelProviderData|null $amazonSfpShippingLabelProviderData
     * @return OutboundShippingLabelProviderData
     */
    public function setAmazonSfpShippingLabelProviderData(
        ?OutboundAmazonSfpShippingLabelProviderData $amazonSfpShippingLabelProviderData
    ): OutboundShippingLabelProviderData {
        $this->amazonSfpShippingLabelProviderData = $amazonSfpShippingLabelProviderData;
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('amazonSfpShippingLabelProviderData', OutboundAmazonSfpShippingLabelProviderData::class, null, true),
        ]);
    }
}
