<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\Merchant\Outbound;

use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\DataModel;

class OutboundAmazonSfpShippingLabelProviderData extends DataModel
{
    /**
     * @var string|null
     */
    protected $sellerId;
    
    /**
     * @return string|null
     */
    public function getSellerId(): ?string
    {
        return $this->sellerId;
    }
    
    /**
     * @param string|null $sellerId
     * @return OutboundAmazonSfpShippingLabelProviderData
     */
    public function setSellerId(?string $sellerId): OutboundAmazonSfpShippingLabelProviderData
    {
        $this->sellerId = $sellerId;
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('sellerId', 'string', null),
        ]);
    }
}
