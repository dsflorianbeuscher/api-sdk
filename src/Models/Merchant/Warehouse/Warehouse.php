<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\Merchant\Warehouse;

use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\General\Address;
use Jtl\Fulfillment\Api\Sdk\Models\General\ModificationInfo;

/**
 * Class Warehouse
 * @package Jtl\Fulfillment\Api\Sdk\Models\Merchant\Warehouse
 */
class Warehouse extends DataModel
{
    /**
     * @var string[]
     */
    protected $shippingMethodIds = [];
    
    /**
     * @var string|null
     */
    protected $fulfillerId;
    
    /**
     * @var ModificationInfo|null
     */
    protected $modificationInfo;
    
    /**
     * @var string|null
     */
    protected $warehouseId;
    
    /**
     * @var string|null
     */
    protected $name;
    
    /**
     * @var Address|null
     */
    protected $address;
    
    /**
     * @return string[]
     */
    public function getShippingMethodIds(): array
    {
        return $this->shippingMethodIds;
    }
    
    /**
     * @param string[] $shippingMethodIds
     * @return Warehouse
     */
    public function setShippingMethodIds(array $shippingMethodIds): Warehouse
    {
        $this->shippingMethodIds = $shippingMethodIds;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getFulfillerId(): ?string
    {
        return $this->fulfillerId;
    }
    
    /**
     * @param string|null $fulfillerId
     * @return Warehouse
     */
    public function setFulfillerId(?string $fulfillerId): Warehouse
    {
        $this->fulfillerId = $fulfillerId;
        
        return $this;
    }
    
    /**
     * @return ModificationInfo|null
     */
    public function getModificationInfo(): ?ModificationInfo
    {
        return $this->modificationInfo;
    }
    
    /**
     * @param ModificationInfo|null $modificationInfo
     * @return Warehouse
     */
    public function setModificationInfo(?ModificationInfo $modificationInfo): Warehouse
    {
        $this->modificationInfo = $modificationInfo;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getWarehouseId(): ?string
    {
        return $this->warehouseId;
    }
    
    /**
     * @param string|null $warehouseId
     * @return Warehouse
     */
    public function setWarehouseId(?string $warehouseId): Warehouse
    {
        $this->warehouseId = $warehouseId;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }
    
    /**
     * @param string|null $name
     * @return Warehouse
     */
    public function setName(?string $name): Warehouse
    {
        $this->name = $name;
        
        return $this;
    }
    
    /**
     * @return Address|null
     */
    public function getAddress(): ?Address
    {
        return $this->address;
    }
    
    /**
     * @param Address $address
     * @return Warehouse
     */
    public function setAddress(Address $address): Warehouse
    {
        $this->address = $address;
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return (new PropertyCollection())->setProperties([
            new PropertyInfo('shippingMethodIds', 'string', [], false, true),
            new PropertyInfo('fulfillerId', 'string', null),
            new PropertyInfo('modificationInfo', ModificationInfo::class, null, true),
            new PropertyInfo('warehouseId', 'string', null),
            new PropertyInfo('name', 'string', null),
            new PropertyInfo('address', Address::class, null, true)
        ]);
    }
}
