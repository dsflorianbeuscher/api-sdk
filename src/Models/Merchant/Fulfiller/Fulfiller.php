<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\Merchant\Fulfiller;

use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\General\Address;
use Jtl\Fulfillment\Api\Sdk\Models\General\ModificationInfo;

/**
 * Class Fulfiller
 * @package Jtl\Fulfillment\Api\Sdk\Models\Merchant\Fulfiller
 */
class Fulfiller extends DataModel
{
    /**
     * @var ModificationInfo|null
     */
    protected $modificationInfo;
    
    /**
     * @var string|null
     */
    protected $userId;
    
    /**
     * @var Address|null
     */
    protected $address;
    
    /**
     * @return ModificationInfo|null
     */
    public function getModificationInfo(): ?ModificationInfo
    {
        return $this->modificationInfo;
    }
    
    /**
     * @param ModificationInfo|null $modificationInfo
     * @return Fulfiller
     */
    public function setModificationInfo(?ModificationInfo $modificationInfo): Fulfiller
    {
        $this->modificationInfo = $modificationInfo;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getUserId(): ?string
    {
        return $this->userId;
    }
    
    /**
     * @param string|null $userId
     * @return Fulfiller
     */
    public function setUserId(?string $userId): Fulfiller
    {
        $this->userId = $userId;
        
        return $this;
    }
    
    /**
     * @return Address
     */
    public function getAddress(): ?Address
    {
        return $this->address;
    }
    
    /**
     * @param Address $address
     * @return Fulfiller
     */
    public function setAddress(Address $address): Fulfiller
    {
        $this->address = $address;
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('modificationInfo', ModificationInfo::class, null, true),
            new PropertyInfo('userId', 'string', null),
            new PropertyInfo('address', Address::class, null, true)
        ]);
    }
}
