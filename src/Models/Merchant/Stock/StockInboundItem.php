<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\Merchant\Stock;

use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\General\Stock\StockInboundItem as GeneralStockInboundItem;

/**
 * Class StockInboundItem
 * @package Jtl\Fulfillment\Api\Sdk\Models\Merchant\Stock
 */
class StockInboundItem extends GeneralStockInboundItem
{
    /**
     * @var string|null
     */
    protected $merchantInboundNumber;
    
    /**
     * @return string|null
     */
    public function getMerchantInboundNumber(): ?string
    {
        return $this->merchantInboundNumber;
    }
    
    /**
     * @param string|null $merchantInboundNumber
     * @return StockInboundItem
     */
    public function setMerchantInboundNumber(?string $merchantInboundNumber): StockInboundItem
    {
        $this->merchantInboundNumber = $merchantInboundNumber;
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return parent::loadProperties()->addProperty(new PropertyInfo('merchantInboundNumber', 'string', null));
    }
}
