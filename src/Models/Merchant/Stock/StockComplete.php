<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\Merchant\Stock;

use Jtl\Fulfillment\Api\Sdk\Models\General\Product\ProductStockLevel;

/**
 * Class StockComplete
 * @package Jtl\Fulfillment\Api\Sdk\Models\Merchant\Stock
 */
class StockComplete extends ProductStockLevel
{
}
