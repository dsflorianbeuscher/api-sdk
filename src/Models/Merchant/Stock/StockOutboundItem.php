<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\Merchant\Stock;

use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\General\Stock\StockOutboundItem as GeneralStockOutboundItem;

/**
 * Class StockOutboundItem
 * @package Jtl\Fulfillment\Api\Sdk\Models\Merchant\Stock
 */
class StockOutboundItem extends GeneralStockOutboundItem
{
    /**
     * @var string|null
     */
    protected $merchantOutboundNumber;
    
    /**
     * @return string|null
     */
    public function getMerchantOutboundNumber(): ?string
    {
        return $this->merchantOutboundNumber;
    }
    
    /**
     * @param string|null $merchantOutboundNumber
     * @return StockOutboundItem
     */
    public function setMerchantOutboundNumber(?string $merchantOutboundNumber): StockOutboundItem
    {
        $this->merchantOutboundNumber = $merchantOutboundNumber;
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return parent::loadProperties()->addProperty(new PropertyInfo('merchantOutboundNumber', 'string', null));
    }
}
