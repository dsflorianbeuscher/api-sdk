<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\Merchant\Stock;

use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\General\Product\Product;
use Jtl\Fulfillment\Api\Sdk\Models\General\Stock\StockChange as GeneralStockChange;

/**
 * Class StockChange
 * @package Jtl\Fulfillment\Api\Sdk\Models\Merchant\Stock
 */
class StockChange extends GeneralStockChange
{
    /**
     * @var StockInboundItem|null
     */
    protected $inboundItem;
    
    /**
     * @var StockOutboundItem|null
     */
    protected $outboundItem;
    
    /**
     * @var Product|null
     */
    protected $product;
    
    /**
     * @return StockInboundItem|null
     */
    public function getInboundItem(): ?StockInboundItem
    {
        return $this->inboundItem;
    }
    
    /**
     * @param StockInboundItem $inboundItem
     * @return StockChange
     */
    public function setInboundItem(StockInboundItem $inboundItem): StockChange
    {
        $this->inboundItem = $inboundItem;
        
        return $this;
    }
    
    /**
     * @return StockOutboundItem|null
     */
    public function getOutboundItem(): ?StockOutboundItem
    {
        return $this->outboundItem;
    }
    
    /**
     * @param StockOutboundItem $outboundItem
     * @return StockChange
     */
    public function setOutboundItem(StockOutboundItem $outboundItem): StockChange
    {
        $this->outboundItem = $outboundItem;
        
        return $this;
    }
    
    /**
     * @return Product|null
     */
    public function getProduct(): ?Product
    {
        return $this->product;
    }
    
    /**
     * @param Product|null $product
     * @return StockChange
     */
    public function setProduct(?Product $product): StockChange
    {
        $this->product = $product;
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return parent::loadProperties()
            ->addProperties([
                new PropertyInfo('inboundItem', StockInboundItem::class, null, true),
                new PropertyInfo('outboundItem', StockOutboundItem::class, null, true),
                new PropertyInfo('product', Product::class, null, true)
            ]);
    }
}
