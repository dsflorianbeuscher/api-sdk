<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\Merchant\Product;

use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\General\Product\Product as GeneralProduct;
use Jtl\Fulfillment\Api\Sdk\Models\Merchant\Inbound\Inbound;
use Jtl\Fulfillment\Api\Sdk\Models\Merchant\Outbound\Outbound;

/**
 * Class Product
 * @package Jtl\Fulfillment\Api\Sdk\Models\Merchant\Product
 */
class Product extends GeneralProduct
{
    /**
     * @var Inbound[]
     */
    protected $openInbounds = [];
    
    /**
     * @var Outbound[]
     */
    protected $openOutbounds = [];
    
    /**
     * @return Inbound[]
     */
    public function getOpenInbounds(): array
    {
        return $this->openInbounds;
    }
    
    /**
     * @param Inbound[] $openInbounds
     * @return Product
     */
    public function setOpenInbounds(array $openInbounds): Product
    {
        $this->openInbounds = $openInbounds;
        
        return $this;
    }
    
    /**
     * @param Inbound $inbound
     * @param string|null $key
     * @return Product
     */
    public function addOpenInbound(Inbound $inbound, ?string $key = null): Product
    {
        if ($key === null) {
            $this->openInbounds[] = $inbound;
        } else {
            $this->openInbounds[$key] = $inbound;
        }
        
        return $this;
    }
    
    /**
     * @return Outbound[]
     */
    public function getOpenOutbounds(): array
    {
        return $this->openOutbounds;
    }
    
    /**
     * @param Outbound[] $openOutbounds
     * @return Product
     */
    public function setOpenOutbounds(array $openOutbounds): Product
    {
        $this->openOutbounds = $openOutbounds;
        
        return $this;
    }
    
    /**
     * @param Outbound $outbound
     * @param string|null $key
     * @return Product
     */
    public function addOpenOutbound(Outbound $outbound, ?string $key = null): Product
    {
        if ($key === null) {
            $this->openOutbounds[] = $outbound;
        } else {
            $this->openOutbounds[$key] = $outbound;
        }
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return parent::loadProperties()->addProperties([
            new PropertyInfo('openInbounds', Inbound::class, [], true, true),
            new PropertyInfo('openOutbounds', Outbound::class, [], true, true)
        ]);
    }
}
