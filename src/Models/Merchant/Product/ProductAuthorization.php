<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\Merchant\Product;

use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\General\ModificationInfo;

/**
 * Class ProductAuthorization
 * @package Jtl\Fulfillment\Api\Sdk\Models\Merchant\Product
 */
class ProductAuthorization extends DataModel
{
    /**
     * @var string|null
     */
    protected $fulfillerId;
    
    /**
     * @var ModificationInfo|null
     */
    protected $modificationInfo;
    
    /**
     * @return string|null
     */
    public function getFulfillerId(): ?string
    {
        return $this->fulfillerId;
    }
    
    /**
     * @param string|null $fulfillerId
     * @return ProductAuthorization
     */
    public function setFulfillerId(?string $fulfillerId): ProductAuthorization
    {
        $this->fulfillerId = $fulfillerId;
        
        return $this;
    }
    
    /**
     * @return ModificationInfo|null
     */
    public function getModificationInfo(): ?ModificationInfo
    {
        return $this->modificationInfo;
    }
    
    /**
     * @param ModificationInfo|null $modificationInfo
     * @return ProductAuthorization
     */
    public function setModificationInfo(?ModificationInfo $modificationInfo): ProductAuthorization
    {
        $this->modificationInfo = $modificationInfo;
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('fulfillerId', 'string', null),
            new PropertyInfo('modificationInfo', ModificationInfo::class, null, true)
        ]);
    }
}
