<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General\Outbound;

use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\General\BestBefore;

/**
 * Class OutboundShippingNotificationItem
 * @package Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Outbound
 */
class OutboundShippingNotificationItem extends DataModel
{
    /**
     * @var string|null
     */
    protected $outboundShippingNotificationItemId;
    
    /**
     * @var string|null
     */
    protected $jfsku;
    
    /**
     * @var float
     */
    protected $quantity;
    
    /**
     * @var string|null
     */
    protected $note;
    
    /**
     * @var BestBefore
     */
    protected $bestBefore;
    
    /**
     * @var string|null
     */
    protected $batch;
    
    /**
     * @var string[]
     */
    protected $serialnumbers = [];
    
    /**
     * @return string|null
     */
    public function getOutboundShippingNotificationItemId(): ?string
    {
        return $this->outboundShippingNotificationItemId;
    }
    
    /**
     * @param string|null $outboundShippingNotificationItemId
     * @return OutboundShippingNotificationItem
     */
    public function setOutboundShippingNotificationItemId(
        string $outboundShippingNotificationItemId
    ): OutboundShippingNotificationItem {
        $this->outboundShippingNotificationItemId = $outboundShippingNotificationItemId;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getJfsku(): ?string
    {
        return $this->jfsku;
    }
    
    /**
     * @param string|null $jfsku
     * @return OutboundShippingNotificationItem
     */
    public function setJfsku(?string $jfsku): OutboundShippingNotificationItem
    {
        $this->jfsku = $jfsku;
        
        return $this;
    }
    
    /**
     * @return float|null
     */
    public function getQuantity(): ?float
    {
        return $this->quantity;
    }
    
    /**
     * @param float|null $quantity
     * @return OutboundShippingNotificationItem
     */
    public function setQuantity(?float $quantity): OutboundShippingNotificationItem
    {
        $this->quantity = $quantity;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getNote(): ?string
    {
        return $this->note;
    }
    
    /**
     * @param string|null $note
     * @return OutboundShippingNotificationItem
     */
    public function setNote(?string $note): OutboundShippingNotificationItem
    {
        $this->note = $note;
        
        return $this;
    }
    
    /**
     * @return BestBefore|null
     */
    public function getBestBefore(): ?BestBefore
    {
        return $this->bestBefore;
    }
    
    /**
     * @param BestBefore $bestBefore
     * @return OutboundShippingNotificationItem
     */
    public function setBestBefore(BestBefore $bestBefore): OutboundShippingNotificationItem
    {
        $this->bestBefore = $bestBefore;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getBatch(): ?string
    {
        return $this->batch;
    }
    
    /**
     * @param string|null $batch
     * @return OutboundShippingNotificationItem
     */
    public function setBatch(?string $batch): OutboundShippingNotificationItem
    {
        $this->batch = $batch;
        
        return $this;
    }
    
    /**
     * @return string[]
     */
    public function getSerialnumbers(): array
    {
        return $this->serialnumbers;
    }
    
    /**
     * @param string[] $serialnumbers
     * @return OutboundShippingNotificationItem
     */
    public function setSerialnumbers(array $serialnumbers): OutboundShippingNotificationItem
    {
        $this->serialnumbers = $serialnumbers;
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('outboundShippingNotificationItemId', 'string', null),
            new PropertyInfo('jfsku', 'string', null),
            new PropertyInfo('quantity', 'float', null),
            new PropertyInfo('note', 'string', null),
            new PropertyInfo('bestBefore', BestBefore::class, null, true),
            new PropertyInfo('batch', 'string', null),
            new PropertyInfo('serialnumbers', 'string', [], false, true)
        ]);
    }
}
