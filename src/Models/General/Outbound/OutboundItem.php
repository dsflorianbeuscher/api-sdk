<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General\Outbound;

use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;

/**
 * Class OutboundItem
 * @package Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Outbound
 */
class OutboundItem extends DataModel
{
    public const ITEM_TYPE_PRODUCT = 'Product';
    public const ITEM_TYPE_SHIPPING = 'Shipping';
    public const ITEM_TYPE_OTHER = 'Other';
    public const ITEM_TYPE_BILL_OF_MATERIALS = 'BillOfMaterials';
    
    /**
     * @var string|null
     */
    protected $itemType = self::ITEM_TYPE_PRODUCT;
    
    /**
     * @var float|null
     */
    protected $quantityOpen;
    
    /**
     * @var string|null
     */
    protected $jfsku;
    
    /**
     * @var string|null
     */
    protected $outboundItemId;
    
    /**
     * @var string|null
     */
    protected $billOfMaterialsId;
    
    /**
     * @var string|null
     */
    protected $name;
    
    /**
     * @var string|null
     */
    protected $merchantSku;
    
    /**
     * @var string|null
     */
    protected $externalNumber;
    
    /**
     * @var float|null
     */
    protected $quantity;
    
    /**
     * @var float|null
     */
    protected $price;
    
    /**
     * @var float|null
     */
    protected $vat;
    
    /**
     * @var bool
     */
    protected $isPartMaster = false;
    
    /**
     * @var string|null
     */
    protected $partItemOutboundItemId;
    
    /**
     * @var string|null
     */
    protected $note;
    
    /**
     * @return string|null
     */
    public function getItemType(): ?string
    {
        return $this->itemType;
    }
    
    /**
     * @param string|null $itemType
     * @return OutboundItem
     */
    public function setItemType(?string $itemType): OutboundItem
    {
        $this->itemType = $itemType;
        
        return $this;
    }
    
    /**
     * @return float|null
     */
    public function getQuantityOpen(): ?float
    {
        return $this->quantityOpen;
    }
    
    /**
     * @param float|null $quantityOpen
     * @return OutboundItem
     */
    public function setQuantityOpen(?float $quantityOpen): OutboundItem
    {
        $this->quantityOpen = $quantityOpen;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getJfsku(): ?string
    {
        return $this->jfsku;
    }
    
    /**
     * @param string|null $jfsku
     * @return OutboundItem
     */
    public function setJfsku(?string $jfsku): OutboundItem
    {
        $this->jfsku = $jfsku;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getOutboundItemId(): ?string
    {
        return $this->outboundItemId;
    }
    
    /**
     * @param string|null $outboundItemId
     * @return OutboundItem
     */
    public function setOutboundItemId(?string $outboundItemId): OutboundItem
    {
        $this->outboundItemId = $outboundItemId;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getBillOfMaterialsId(): ?string
    {
        return $this->billOfMaterialsId;
    }
    
    /**
     * @param string|null $billOfMaterialsId
     * @return OutboundItem
     */
    public function setBillOfMaterialsId(?string $billOfMaterialsId): OutboundItem
    {
        $this->billOfMaterialsId = $billOfMaterialsId;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }
    
    /**
     * @param string|null $name
     * @return OutboundItem
     */
    public function setName(?string $name): OutboundItem
    {
        $this->name = $name;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getMerchantSku(): ?string
    {
        return $this->merchantSku;
    }
    
    /**
     * @param string|null $merchantSku
     * @return OutboundItem
     */
    public function setMerchantSku(?string $merchantSku): OutboundItem
    {
        $this->merchantSku = $merchantSku;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getExternalNumber(): ?string
    {
        return $this->externalNumber;
    }
    
    /**
     * @param string|null $externalNumber
     * @return OutboundItem
     */
    public function setExternalNumber(?string $externalNumber): OutboundItem
    {
        $this->externalNumber = $externalNumber;
        
        return $this;
    }
    
    /**
     * @return float|null
     */
    public function getQuantity(): ?float
    {
        return $this->quantity;
    }
    
    /**
     * @param float|null $quantity
     * @return OutboundItem
     */
    public function setQuantity(?float $quantity): OutboundItem
    {
        $this->quantity = $quantity;
        
        return $this;
    }
    
    /**
     * @return float|null
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }
    
    /**
     * @param float|null $price
     * @return OutboundItem
     */
    public function setPrice(?float $price): OutboundItem
    {
        $this->price = $price;
        
        return $this;
    }
    
    /**
     * @return float|null
     */
    public function getVat(): ?float
    {
        return $this->vat;
    }
    
    /**
     * @param float|null $vat
     * @return OutboundItem
     */
    public function setVat(?float $vat): OutboundItem
    {
        $this->vat = $vat;
        
        return $this;
    }
    
    /**
     * @return bool
     */
    public function isPartMaster(): bool
    {
        return $this->isPartMaster;
    }
    
    /**
     * @param bool $isPartMaster
     * @return OutboundItem
     */
    public function setIsPartMaster(bool $isPartMaster): OutboundItem
    {
        $this->isPartMaster = $isPartMaster;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getPartItemOutboundItemId(): ?string
    {
        return $this->partItemOutboundItemId;
    }
    
    /**
     * @param string|null $partItemOutboundItemId
     * @return OutboundItem
     */
    public function setPartItemOutboundItemId(?string $partItemOutboundItemId): OutboundItem
    {
        $this->partItemOutboundItemId = $partItemOutboundItemId;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getNote(): ?string
    {
        return $this->note;
    }
    
    /**
     * @param string|null $note
     * @return OutboundItem
     */
    public function setNote(?string $note): OutboundItem
    {
        $this->note = $note;
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('itemType', 'string', self::ITEM_TYPE_PRODUCT),
            new PropertyInfo('quantityOpen', 'float', null),
            new PropertyInfo('jfsku', 'string', null),
            new PropertyInfo('outboundItemId', 'string', null),
            new PropertyInfo('billOfMaterialsId', 'string', null),
            new PropertyInfo('name', 'string', null),
            new PropertyInfo('merchantSku', 'string', null),
            new PropertyInfo('externalNumber', 'string', null),
            new PropertyInfo('quantity', 'float', null),
            new PropertyInfo('price', 'float', null),
            new PropertyInfo('vat', 'float', null),
            new PropertyInfo('isPartMaster', 'bool', false),
            new PropertyInfo('partItemOutboundItemId', 'string', null),
            new PropertyInfo('note', 'string', null)
        ]);
    }
}
