<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General\Outbound;

use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\General\Package\Package;

/**
 * Class OutboundShippingNotification
 * @package Jtl\Fulfillment\Api\Sdk\Models\General\Outbound
 */
class OutboundShippingNotification extends DataModel
{
    /**
     * @var string|null
     */
    protected $outboundId;
    
    /**
     * @var string|null
     */
    protected $outboundShippingNotificationId;
    
    /**
     * @var OutboundShippingNotificationItem[]
     */
    protected $items = [];
    
    /**
     * @var Package[]
     */
    protected $packages = [];
    
    /**
     * @var string|null
     */
    protected $fulfillerShippingNotificationNumber;
    
    /**
     * @var string|null
     */
    protected $note;
    
    /**
     * @var string|null
     */
    protected $outboundStatus;
    
    /**
     * @return string|null
     */
    public function getOutboundId(): ?string
    {
        return $this->outboundId;
    }
    
    /**
     * @param string|null $outboundId
     * @return OutboundShippingNotification
     */
    public function setOutboundId(?string $outboundId): OutboundShippingNotification
    {
        $this->outboundId = $outboundId;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getOutboundShippingNotificationId(): ?string
    {
        return $this->outboundShippingNotificationId;
    }
    
    /**
     * @param string|null $outboundShippingNotificationId
     * @return OutboundShippingNotification
     */
    public function setOutboundShippingNotificationId(
        string $outboundShippingNotificationId
    ): OutboundShippingNotification {
        $this->outboundShippingNotificationId = $outboundShippingNotificationId;
        
        return $this;
    }
    
    /**
     * @return OutboundShippingNotificationItem[]
     */
    public function getItems(): array
    {
        return $this->items;
    }
    
    /**
     * @param OutboundShippingNotificationItem[] $items
     * @return OutboundShippingNotification
     */
    public function setItems(array $items): OutboundShippingNotification
    {
        $this->items = $items;
        
        return $this;
    }
    
    /**
     * @param OutboundShippingNotificationItem $item
     * @param string|null $key
     * @return OutboundShippingNotification
     */
    public function addItem(OutboundShippingNotificationItem $item, ?string $key = null): OutboundShippingNotification
    {
        if ($key === null) {
            $this->items[] = $item;
        } else {
            $this->items[$key] = $item;
        }
        
        return $this;
    }
    
    /**
     * @return Package[]
     */
    public function getPackages(): array
    {
        return $this->packages;
    }
    
    /**
     * @param Package[] $packages
     * @return OutboundShippingNotification
     */
    public function setPackages(array $packages): OutboundShippingNotification
    {
        $this->packages = $packages;
        
        return $this;
    }
    
    /**
     * @param Package $package
     * @param string|null $key
     * @return OutboundShippingNotification
     */
    public function addPackage(Package $package, ?string $key = null): OutboundShippingNotification
    {
        if ($key === null) {
            $this->packages[] = $package;
        } else {
            $this->packages[$key] = $package;
        }
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getFulfillerShippingNotificationNumber(): ?string
    {
        return $this->fulfillerShippingNotificationNumber;
    }
    
    /**
     * @param string|null $fulfillerShippingNotificationNumber
     * @return OutboundShippingNotification
     */
    public function setFulfillerShippingNotificationNumber(
        string $fulfillerShippingNotificationNumber
    ): OutboundShippingNotification {
        $this->fulfillerShippingNotificationNumber = $fulfillerShippingNotificationNumber;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getNote(): ?string
    {
        return $this->note;
    }
    
    /**
     * @param string|null $note
     * @return OutboundShippingNotification
     */
    public function setNote(?string $note): OutboundShippingNotification
    {
        $this->note = $note;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getOutboundStatus(): ?string
    {
        return $this->outboundStatus;
    }
    
    /**
     * @param string|null $outboundStatus
     * @return OutboundShippingNotification
     */
    public function setOutboundStatus(?string $outboundStatus): OutboundShippingNotification
    {
        $this->outboundStatus = $outboundStatus;
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('outboundId', 'string', null),
            new PropertyInfo('outboundShippingNotificationId', 'string', null),
            new PropertyInfo('items', OutboundShippingNotificationItem::class, [], true, true),
            new PropertyInfo('packages', Package::class, [], true, true),
            new PropertyInfo('fulfillerShippingNotificationNumber', 'string', null),
            new PropertyInfo('note', 'string', null),
            new PropertyInfo('outboundStatus', 'string', null),
        ]);
    }
}
