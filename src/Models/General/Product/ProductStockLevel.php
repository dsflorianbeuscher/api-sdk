<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General\Product;

use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\General\Stock\StockLevel;

/**
 * Class ProductStockLevel
 * @package Jtl\Fulfillment\Api\Sdk\Models\Fulfiller
 */
class ProductStockLevel extends StockLevel
{
    /**
     * @var string|null
     */
    protected $merchantSku;
    
    /**
     * @var ProductStockLevelWarehouse[]
     */
    protected $warehouses = [];
    
    /**
     * @return string|null
     */
    public function getMerchantSku(): ?string
    {
        return $this->merchantSku;
    }
    
    /**
     * @param string|null $merchantSku
     * @return ProductStockLevel
     */
    public function setMerchantSku(?string $merchantSku): ProductStockLevel
    {
        $this->merchantSku = $merchantSku;
        
        return $this;
    }
    
    /**
     * @return ProductStockLevelWarehouse[]
     */
    public function getWarehouses(): array
    {
        return $this->warehouses;
    }
    
    /**
     * @param ProductStockLevelWarehouse[] $warehouses
     * @return ProductStockLevel
     */
    public function setWarehouses(array $warehouses): ProductStockLevel
    {
        $this->warehouses = $warehouses;
        
        return $this;
    }
    
    /**
     * @param ProductStockLevelWarehouse $warehouse
     * @param string|null $key
     * @return ProductStockLevel
     */
    public function addWarehouse(ProductStockLevelWarehouse $warehouse, ?string $key = null): ProductStockLevel
    {
        if ($key === null) {
            $this->warehouses[] = $warehouse;
        } else {
            $this->warehouses[$key] = $warehouse;
        }
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return parent::loadProperties()->addProperties([
            new PropertyInfo('merchantSku', 'string', null),
            new PropertyInfo('warehouses', ProductStockLevelWarehouse::class, [], true, true)
        ]);
    }
}
