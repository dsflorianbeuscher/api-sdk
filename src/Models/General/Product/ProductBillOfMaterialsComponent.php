<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General\Product;

use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;

/**
 * Class ProductBillOfMaterialsComponent
 * @package Jtl\Fulfillment\Api\Sdk\Models\General\Product
 */
class ProductBillOfMaterialsComponent extends DataModel
{
    /**
     * @var string|null
     */
    protected $jfsku;
    
    /**
     * @var float|null
     */
    protected $quantity;
    
    /**
     * @var string|null
     */
    protected $merchantSku;
    
    /**
     * @var string|null
     */
    protected $name;
    
    /**
     * @return string|null
     */
    public function getJfsku(): ?string
    {
        return $this->jfsku;
    }
    
    /**
     * @param string|null $jfsku
     * @return ProductBillOfMaterialsComponent
     */
    public function setJfsku(?string $jfsku): ProductBillOfMaterialsComponent
    {
        $this->jfsku = $jfsku;
        
        return $this;
    }
    
    /**
     * @return float|null
     */
    public function getQuantity(): ?float
    {
        return $this->quantity;
    }
    
    /**
     * @param float|null $quantity
     * @return ProductBillOfMaterialsComponent
     */
    public function setQuantity(?float $quantity): ProductBillOfMaterialsComponent
    {
        $this->quantity = $quantity;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getMerchantSku(): ?string
    {
        return $this->merchantSku;
    }
    
    /**
     * @param string|null $merchantSku
     * @return ProductBillOfMaterialsComponent
     */
    public function setMerchantSku(?string $merchantSku): ProductBillOfMaterialsComponent
    {
        $this->merchantSku = $merchantSku;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }
    
    /**
     * @param string|null $name
     * @return ProductBillOfMaterialsComponent
     */
    public function setName(?string $name): ProductBillOfMaterialsComponent
    {
        $this->name = $name;
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('jfsku', 'string', null),
            new PropertyInfo('quantity', 'float', null),
            new PropertyInfo('merchantSku', 'string', null),
            new PropertyInfo('name', 'string', null),
        ]);
    }
}
