<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General;

use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;

/**
 * Class ShippingMethod
 * @package Jtl\Fulfillment\Api\Sdk\Models\General
 */
class ShippingMethod extends DataModel
{
    /**
     * @var string|null
     */
    protected $shippingMethodId;
    
    /**
     * @var ModificationInfo|null
     */
    protected $modificationInfo;
    
    /**
     * @var string|null
     */
    protected $name;
    
    /**
     * @var string|null
     */
    protected $trackingUrlSchema;
    
    /**
     * @var string|null
     */
    protected $carrierCode;
    
    /**
     * @var string|null
     */
    protected $carrierName;
    
    /**
     * @var string|null
     */
    protected $note;
    
    /**
     * @var string|null
     */
    protected $cutoffTime;
    
    /**
     * @var string|null
     */
    protected $shippingType;
    
    /**
     * @return string|null
     */
    public function getShippingMethodId(): ?string
    {
        return $this->shippingMethodId;
    }
    
    /**
     * @param string|null $shippingMethodId
     * @return ShippingMethod
     */
    public function setShippingMethodId(?string $shippingMethodId): ShippingMethod
    {
        $this->shippingMethodId = $shippingMethodId;
        
        return $this;
    }
    
    /**
     * @return ModificationInfo|null
     */
    public function getModificationInfo(): ?ModificationInfo
    {
        return $this->modificationInfo;
    }
    
    /**
     * @param ModificationInfo|null $modificationInfo
     * @return ShippingMethod
     */
    public function setModificationInfo(?ModificationInfo $modificationInfo): ShippingMethod
    {
        $this->modificationInfo = $modificationInfo;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }
    
    /**
     * @param string|null $name
     * @return ShippingMethod
     */
    public function setName(?string $name): ShippingMethod
    {
        $this->name = $name;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getTrackingUrlSchema(): ?string
    {
        return $this->trackingUrlSchema;
    }
    
    /**
     * @param string|null $trackingUrlSchema
     * @return ShippingMethod
     */
    public function setTrackingUrlSchema(?string $trackingUrlSchema): ShippingMethod
    {
        $this->trackingUrlSchema = $trackingUrlSchema;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getCarrierCode(): ?string
    {
        return $this->carrierCode;
    }
    
    /**
     * @param string|null $carrierCode
     * @return ShippingMethod
     */
    public function setCarrierCode(?string $carrierCode): ShippingMethod
    {
        $this->carrierCode = $carrierCode;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getCarrierName(): ?string
    {
        return $this->carrierName;
    }
    
    /**
     * @param string|null $carrierName
     * @return ShippingMethod
     */
    public function setCarrierName(?string $carrierName): ShippingMethod
    {
        $this->carrierName = $carrierName;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getNote(): ?string
    {
        return $this->note;
    }
    
    /**
     * @param string|null $note
     * @return ShippingMethod
     */
    public function setNote(?string $note): ShippingMethod
    {
        $this->note = $note;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getCutoffTime(): ?string
    {
        return $this->cutoffTime;
    }
    
    /**
     * @param string|null $cutoffTime
     * @return ShippingMethod
     */
    public function setCutoffTime(?string $cutoffTime): ShippingMethod
    {
        $this->cutoffTime = $cutoffTime;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getShippingType(): ?string
    {
        return $this->shippingType;
    }
    
    /**
     * @param string|null $shippingType
     * @return ShippingMethod
     */
    public function setShippingType(?string $shippingType): ShippingMethod
    {
        $this->shippingType = $shippingType;
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return (new PropertyCollection())->setProperties([
            new PropertyInfo('shippingMethodId', 'string', null),
            new PropertyInfo('modificationInfo', ModificationInfo::class, null, true),
            new PropertyInfo('name', 'string', null),
            new PropertyInfo('trackingUrlSchema', 'string', null),
            new PropertyInfo('carrierCode', 'string', null),
            new PropertyInfo('carrierName', 'string', null),
            new PropertyInfo('note', 'string', null),
            new PropertyInfo('cutoffTime', 'string', null),
            new PropertyInfo('shippingType', 'string', null)
        ]);
    }
}
