<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General\Returns;

use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Jtl\Fulfillment\Api\Sdk\Models\General\AddressChange;
use Jtl\Fulfillment\Api\Sdk\Models\General\ChangeValue;

class ReturnChange extends DataModel
{
    /**
     * @var ChangeValue|null
     */
    protected $returnId;

    /**
     * @var ChangeValue|null
     */
    protected $warehouseId;

    /**
     * @var ChangeValue|null
     */
    protected $merchantReturnNumber;

    /**
     * @var ChangeValue|null
     */
    protected $fulfillerReturnNumber;

    /**
     * @var ChangeValue|null
     */
    protected $contact;

    /**
     * @var ChangeValue|null
     */
    protected $internalNote;

    /**
     * @var ChangeValue|null
     */
    protected $externalNote;

    /**
     * @var AddressChange|null
     */
    protected $customerAddress;

    /**
     * @var ChangeValue|null
     */
    protected $state;

    /**
     * @var WriteLockChange|null
     */
    protected $writeLock;

    /**
     * @var ReturnChangeItemShell[]
     */
    protected $items = [];

    /**
     * @return ChangeValue|null
     */
    public function getReturnId(): ?ChangeValue
    {
        return $this->returnId;
    }

    /**
     * @param ChangeValue|null $returnId
     * @return ReturnChange
     */
    public function setReturnId(?ChangeValue $returnId): ReturnChange
    {
        $this->returnId = $returnId;
        return $this;
    }

    /**
     * @return ChangeValue|null
     */
    public function getWarehouseId(): ?ChangeValue
    {
        return $this->warehouseId;
    }

    /**
     * @param ChangeValue|null $warehouseId
     * @return ReturnChange
     */
    public function setWarehouseId(?ChangeValue $warehouseId): ReturnChange
    {
        $this->warehouseId = $warehouseId;
        return $this;
    }

    /**
     * @return ChangeValue|null
     */
    public function getMerchantReturnNumber(): ?ChangeValue
    {
        return $this->merchantReturnNumber;
    }

    /**
     * @param ChangeValue|null $merchantReturnNumber
     * @return ReturnChange
     */
    public function setMerchantReturnNumber(?ChangeValue $merchantReturnNumber): ReturnChange
    {
        $this->merchantReturnNumber = $merchantReturnNumber;
        return $this;
    }

    /**
     * @return ChangeValue|null
     */
    public function getFulfillerReturnNumber(): ?ChangeValue
    {
        return $this->fulfillerReturnNumber;
    }

    /**
     * @param ChangeValue|null $fulfillerReturnNumber
     * @return ReturnChange
     */
    public function setFulfillerReturnNumber(?ChangeValue $fulfillerReturnNumber): ReturnChange
    {
        $this->fulfillerReturnNumber = $fulfillerReturnNumber;
        return $this;
    }

    /**
     * @return ChangeValue|null
     */
    public function getContact(): ?ChangeValue
    {
        return $this->contact;
    }

    /**
     * @param ChangeValue|null $contact
     * @return ReturnChange
     */
    public function setContact(?ChangeValue $contact): ReturnChange
    {
        $this->contact = $contact;
        return $this;
    }

    /**
     * @return ChangeValue|null
     */
    public function getInternalNote(): ?ChangeValue
    {
        return $this->internalNote;
    }

    /**
     * @param ChangeValue|null $internalNote
     * @return ReturnChange
     */
    public function setInternalNote(?ChangeValue $internalNote): ReturnChange
    {
        $this->internalNote = $internalNote;
        return $this;
    }

    /**
     * @return ChangeValue|null
     */
    public function getExternalNote(): ?ChangeValue
    {
        return $this->externalNote;
    }

    /**
     * @param ChangeValue|null $externalNote
     * @return ReturnChange
     */
    public function setExternalNote(?ChangeValue $externalNote): ReturnChange
    {
        $this->externalNote = $externalNote;
        return $this;
    }

    /**
     * @return AddressChange|null
     */
    public function getCustomerAddress(): ?AddressChange
    {
        return $this->customerAddress;
    }

    /**
     * @param AddressChange|null $customerAddress
     * @return ReturnChange
     */
    public function setCustomerAddress(?AddressChange $customerAddress): ReturnChange
    {
        $this->customerAddress = $customerAddress;
        return $this;
    }

    /**
     * @return ChangeValue|null
     */
    public function getState(): ?ChangeValue
    {
        return $this->state;
    }

    /**
     * @param ChangeValue|null $state
     * @return ReturnChange
     */
    public function setState(?ChangeValue $state): ReturnChange
    {
        $this->state = $state;
        return $this;
    }

    /**
     * @return WriteLockChange|null
     */
    public function getWriteLock(): ?WriteLockChange
    {
        return $this->writeLock;
    }

    /**
     * @param WriteLockChange|null $writeLock
     * @return ReturnChange
     */
    public function setWriteLock(?WriteLockChange $writeLock): ReturnChange
    {
        $this->writeLock = $writeLock;
        return $this;
    }

    /**
     * @return ReturnChangeItemShell[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param ReturnChangeItemShell[] $items
     * @return ReturnChange
     */
    public function setItems(array $items): ReturnChange
    {
        $this->items = $items;
        return $this;
    }

    /**
     * @param ReturnChangeItemShell $item
     * @param string|null $key
     * @return ReturnChange
     */
    public function addItem(ReturnChangeItemShell $item, string $key = null): ReturnChange
    {
        if ($key === null) {
            $this->items[] = $item;
        } else {
            $this->items[$key] = $item;
        }

        return $this;
    }

    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('returnId', ChangeValue::class, null, true),
            new PropertyInfo('warehouseId', ChangeValue::class, null, true),
            new PropertyInfo('merchantReturnNumber', ChangeValue::class, null, true),
            new PropertyInfo('fulfillerReturnNumber', ChangeValue::class, null, true),
            new PropertyInfo('contact', ChangeValue::class, null, true),
            new PropertyInfo('internalNote', ChangeValue::class, null, true),
            new PropertyInfo('externalNote', ChangeValue::class, null, true),
            new PropertyInfo('customerAddress', AddressChange::class, null, true),
            new PropertyInfo('state', ChangeValue::class, null, true),
            new PropertyInfo('writeLock', WriteLockChange::class, null, true),
            new PropertyInfo('items', ReturnChangeItemShell::class, [], true, true),
        ]);
    }
}
