<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General\Returns;

use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\DataModel;

/**
 * Class ReturnStockChange
 * @package Jtl\Fulfillment\Api\Sdk\Models\General\Returns
 */
class ReturnStockChange extends DataModel
{
    /**
     * @var ReturnStockChangeId|null
     */
    protected $stockChangeId;

    /**
     * @var string|null
     */
    protected $merchantSku;

    /**
     * @var float|null
     */
    protected $quantity;

    /**
     * @var float|null
     */
    protected $quantityBlocked;

    /**
     * @return ReturnStockChangeId|null
     */
    public function getStockChangeId(): ?ReturnStockChangeId
    {
        return $this->stockChangeId;
    }

    /**
     * @param ReturnStockChangeId|null $stockChangeId
     * @return ReturnStockChange
     */
    public function setStockChangeId(?ReturnStockChangeId $stockChangeId): ReturnStockChange
    {
        $this->stockChangeId = $stockChangeId;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMerchantSku(): ?string
    {
        return $this->merchantSku;
    }

    /**
     * @param string|null $merchantSku
     * @return ReturnStockChange
     */
    public function setMerchantSku(?string $merchantSku): ReturnStockChange
    {
        $this->merchantSku = $merchantSku;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getQuantity(): ?float
    {
        return $this->quantity;
    }

    /**
     * @param float|null $quantity
     * @return ReturnStockChange
     */
    public function setQuantity(?float $quantity): ReturnStockChange
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getQuantityBlocked(): ?float
    {
        return $this->quantityBlocked;
    }

    /**
     * @param float|null $quantityBlocked
     * @return ReturnStockChange
     */
    public function setQuantityBlocked(?float $quantityBlocked): ReturnStockChange
    {
        $this->quantityBlocked = $quantityBlocked;
        return $this;
    }

    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('stockChangeId', ReturnStockChangeId::class, null, true),
            new PropertyInfo('merchantSku', 'string', null),
            new PropertyInfo('quantity', 'float', null),
            new PropertyInfo('quantityBlocked', 'float', null),
        ]);
    }
}
