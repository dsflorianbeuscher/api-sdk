<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General\Returns;

use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Jtl\Fulfillment\Api\Sdk\Models\General\ChangeValue;

/**
 * Class WriteLockChange
 * @package Jtl\Fulfillment\Api\Sdk\Models\General\Returns
 */
class WriteLockChange extends DataModel
{
    /**
     * @var ChangeValue|null
     */
    protected $lockedByUserId;

    /**
     * @var ChangeValue|null
     */
    protected $lockedAt;

    /**
     * @var ChangeValue|null
     */
    protected $lockedUntil;

    /**
     * @return ChangeValue|null
     */
    public function getLockedByUserId(): ?ChangeValue
    {
        return $this->lockedByUserId;
    }

    /**
     * @param ChangeValue|null $lockedByUserId
     * @return WriteLockChange
     */
    public function setLockedByUserId(?ChangeValue $lockedByUserId): WriteLockChange
    {
        $this->lockedByUserId = $lockedByUserId;
        return $this;
    }

    /**
     * @return ChangeValue|null
     */
    public function getLockedAt(): ?ChangeValue
    {
        return $this->lockedAt;
    }

    /**
     * @param ChangeValue|null $lockedAt
     * @return WriteLockChange
     */
    public function setLockedAt(?ChangeValue $lockedAt): WriteLockChange
    {
        $this->lockedAt = $lockedAt;
        return $this;
    }

    /**
     * @return ChangeValue|null
     */
    public function getLockedUntil(): ?ChangeValue
    {
        return $this->lockedUntil;
    }

    /**
     * @param ChangeValue|null $lockedUntil
     * @return WriteLockChange
     */
    public function setLockedUntil(?ChangeValue $lockedUntil): WriteLockChange
    {
        $this->lockedUntil = $lockedUntil;
        return $this;
    }

    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('lockedByUserId', ChangeValue::class, null, true),
            new PropertyInfo('lockedAt', ChangeValue::class, null, true),
            new PropertyInfo('lockedUntil', ChangeValue::class, null, true),
        ]);
    }
}
