<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General\Returns;

use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Jtl\Fulfillment\Api\Sdk\Models\General\ChangeValue;

/**
 * Class ReturnStockChangeId
 * @package Jtl\Fulfillment\Api\Sdk\Models\General\Returns
 */
class ReturnStockChangeId extends DataModel
{
    /**
     * @var string|null
     */
    protected $warehouseId;

    /**
     * @var string|null
     */
    protected $jfsku;

    /**
     * @var int|null
     */
    protected $stockVersion;

    /**
     * @return string|null
     */
    public function getWarehouseId(): ?string
    {
        return $this->warehouseId;
    }

    /**
     * @param string|null $warehouseId
     * @return ReturnStockChangeId
     */
    public function setWarehouseId(?string $warehouseId): ReturnStockChangeId
    {
        $this->warehouseId = $warehouseId;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getJfsku(): ?string
    {
        return $this->jfsku;
    }

    /**
     * @param string|null $jfsku
     * @return ReturnStockChangeId
     */
    public function setJfsku(?string $jfsku): ReturnStockChangeId
    {
        $this->jfsku = $jfsku;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getStockVersion(): ?int
    {
        return $this->stockVersion;
    }

    /**
     * @param int|null $stockVersion
     * @return ReturnStockChangeId
     */
    public function setStockVersion(?int $stockVersion): ReturnStockChangeId
    {
        $this->stockVersion = $stockVersion;
        return $this;
    }

    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('warehouseId', 'string', null),
            new PropertyInfo('jfsku', 'string', null),
            new PropertyInfo('stockVersion', 'int', null),
        ]);
    }
}
