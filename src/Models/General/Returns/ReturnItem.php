<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General\Returns;

use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\DataModel;

/**
 * Class BaseReturn
 * @package Jtl\Fulfillment\Api\Sdk\Models\General\Returns
 */
class ReturnItem extends DataModel
{
    public const REASON_NO_REASON = 'NoReason';
    public const REASON_ORDERED_BY_MISTAKE = 'OrderedByMistake';
    public const REASON_FOUND_CHEAPER_PRICE = 'FoundCheaperPrice';
    public const REASON_INSUFFICIENT_PERFORMANCE_OR_QUALITY = 'InsufficientPerformanceOrQuality';
    public const REASON_INCOMPATIBLE_OR_NOT_APPROPRIATE_FOR_INTENDED_USE = 'IncompatibleOrNotAppropriateForIntendedUse';
    public const REASON_SHIPPING_PACKAGING_INTACT_ITEM_DAMAGED = 'ShippingPackagingIntactItemDamaged';
    public const REASON_ARRIVED_TOO_LATE = 'ArrivedTooLate';
    public const REASON_PARTS_ACCESSORIES_MISSING = 'PartsAccessoriesMissing';
    public const REASON_SHIPPING_PACKAGE_AND_ITEM_DAMAGED = 'ShippingPackageAndItemDamaged';
    public const REASON_WRONG_ITEM_SENT = 'WrongItemSent';
    public const REASON_DEFECTIVE_DOES_NOT_WORK_PERFECTLY = 'DefectiveDoesNotWorkPerfectly';
    public const REASON_EXCESS_DELIVERY = 'ExcessDelivery';
    public const REASON_DO_NOT_LIKE_ANYMORE = 'DoNotLikeAnymore';
    public const REASON_UNAUTHORISED_PURCHASE = 'UnauthorisedPurchase';
    public const REASON_DOES_NOT_MATCH_DESCRIPTION_ON_WEBSITE = 'DoesNotMatchDescriptionOnWebsite';
    public const REASON_RECIPIENT_COULD_NOT_BE_DETERMINED = 'RecipientCouldNotBeDetermined';
    public const REASON_PICK_UP_TIME_EXPIRED = 'PickUpTimeExpired';

    /**
     * @var string|null
     */
    protected $returnItemId;

    /**
     * @var string|null
     */
    protected $jfsku;

    /**
     * @var string|null
     */
    protected $name;

    /**
     * @var string|null
     */
    protected $outboundId;

    /**
     * @var string|null
     */
    protected $outboundItemId;

    /**
     * @var float|null
     */
    protected $quantity;

    /**
     * @var string|null
     */
    protected $reason;

    /**
     * @var string|null
     */
    protected $reasonNote;

    /**
     * @var string|null
     */
    protected $conditionNote;

    /**
     * @var string|null
     */
    protected $state;

    /**
     * @var string|null
     */
    protected $condition;

    /**
     * @return string|null
     */
    public function getReturnItemId(): ?string
    {
        return $this->returnItemId;
    }

    /**
     * @param string|null $returnItemId
     * @return ReturnItem
     */
    public function setReturnItemId(?string $returnItemId): ReturnItem
    {
        $this->returnItemId = $returnItemId;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getJfsku(): ?string
    {
        return $this->jfsku;
    }

    /**
     * @param string|null $jfsku
     * @return ReturnItem
     */
    public function setJfsku(?string $jfsku): ReturnItem
    {
        $this->jfsku = $jfsku;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return ReturnItem
     */
    public function setName(?string $name): ReturnItem
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getOutboundId(): ?string
    {
        return $this->outboundId;
    }

    /**
     * @param string|null $outboundId
     * @return ReturnItem
     */
    public function setOutboundId(?string $outboundId): ReturnItem
    {
        $this->outboundId = $outboundId;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getOutboundItemId(): ?string
    {
        return $this->outboundItemId;
    }

    /**
     * @param string|null $outboundItemId
     * @return ReturnItem
     */
    public function setOutboundItemId(?string $outboundItemId): ReturnItem
    {
        $this->outboundItemId = $outboundItemId;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getQuantity(): ?float
    {
        return $this->quantity;
    }

    /**
     * @param float|null $quantity
     * @return ReturnItem
     */
    public function setQuantity(?float $quantity): ReturnItem
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getReason(): ?string
    {
        return $this->reason;
    }

    /**
     * @param string|null $reason
     * @return ReturnItem
     */
    public function setReason(?string $reason): ReturnItem
    {
        $this->reason = $reason;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getReasonNote(): ?string
    {
        return $this->reasonNote;
    }

    /**
     * @param string|null $reasonNote
     * @return ReturnItem
     */
    public function setReasonNote(?string $reasonNote): ReturnItem
    {
        $this->reasonNote = $reasonNote;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getConditionNote(): ?string
    {
        return $this->conditionNote;
    }

    /**
     * @param string|null $conditionNote
     * @return ReturnItem
     */
    public function setConditionNote(?string $conditionNote): ReturnItem
    {
        $this->conditionNote = $conditionNote;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getState(): ?string
    {
        return $this->state;
    }

    /**
     * @param string|null $state
     * @return ReturnItem
     */
    public function setState(?string $state): ReturnItem
    {
        $this->state = $state;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCondition(): ?string
    {
        return $this->condition;
    }

    /**
     * @param string|null $condition
     * @return ReturnItem
     */
    public function setCondition(?string $condition): ReturnItem
    {
        $this->condition = $condition;
        return $this;
    }

    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('returnItemId', 'string', null),
            new PropertyInfo('jfsku', 'string', null),
            new PropertyInfo('name', 'string', null),
            new PropertyInfo('outboundId', 'string', null),
            new PropertyInfo('outboundItemId', 'string', null),
            new PropertyInfo('quantity', 'float', null),
            new PropertyInfo('reason', 'string', null),
            new PropertyInfo('reasonNote', 'string', null),
            new PropertyInfo('conditionNote', 'string', null),
            new PropertyInfo('state', 'string', null),
            new PropertyInfo('condition', 'string', null),
        ]);
    }
}
