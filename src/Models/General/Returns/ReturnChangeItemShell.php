<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General\Returns;

use DateTimeZone;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use DateTime;
use Exception;

/**
 * Class ReturnChangeItemShell
 * @package Jtl\Fulfillment\Api\Sdk\Models\General\Returns
 */
class ReturnChangeItemShell extends DataModel
{
    /**
     * @var string|null
     */
    protected $id;

    /**
     * @var string|null
     */
    protected $modificationState;

    /**
     * @var string|null
     */
    protected $modifiedFrom;

    /**
     * @var DateTime|null
     */
    protected $updatedAt;

    /**
     * @var ReturnChangeItem|null
     */
    protected $changes;

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param string|null $id
     * @return ReturnChangeItemShell
     */
    public function setId(?string $id): ReturnChangeItemShell
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getModificationState(): ?string
    {
        return $this->modificationState;
    }

    /**
     * @param string|null $modificationState
     * @return ReturnChangeItemShell
     */
    public function setModificationState(?string $modificationState): ReturnChangeItemShell
    {
        $this->modificationState = $modificationState;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getModifiedFrom(): ?string
    {
        return $this->modifiedFrom;
    }

    /**
     * @param string|null $modifiedFrom
     * @return ReturnChangeItemShell
     */
    public function setModifiedFrom(?string $modifiedFrom): ReturnChangeItemShell
    {
        $this->modifiedFrom = $modifiedFrom;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getUpdatedAt(): ?DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param DateTime|string|null $updatedAt
     * @return ReturnChangeItemShell
     * @throws Exception
     */
    public function setUpdatedAt($updatedAt): ReturnChangeItemShell
    {
        if ($updatedAt === null) {
            return $this;
        }

        if (is_string($updatedAt)) {
            $updatedAt = (new DateTime($updatedAt))->setTimezone(new DateTimeZone('UTC'));
        }

        $this->checkDate($updatedAt);
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return ReturnChangeItem|null
     */
    public function getChanges(): ?ReturnChangeItem
    {
        return $this->changes;
    }

    /**
     * @param ReturnChangeItem $changes
     * @return ReturnChangeItemShell
     */
    public function setChanges(ReturnChangeItem $changes): ReturnChangeItemShell
    {
        $this->changes = $changes;
        return $this;
    }

    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('id', 'string', null),
            new PropertyInfo('modificationState', 'string', null),
            new PropertyInfo('modifiedFrom', 'string', null),
            new PropertyInfo('updatedAt', DateTime::class, null),
            new PropertyInfo('changes', ReturnChangeItem::class, null, true),
        ]);
    }
}
