<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General\Returns;

use DateTimeZone;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use DateTime;
use Exception;

/**
 * Class ReturnChangeShell
 * @package Jtl\Fulfillment\Api\Sdk\Models\General\Returns
 */
class ReturnChangeShell extends DataModel
{
    /**
     * @var string|null
     */
    protected $id;

    /**
     * @var string|null
     */
    protected $modificationState;

    /**
     * @var string|null
     */
    protected $modifiedFrom;

    /**
     * @var DateTime|null
     */
    protected $updatedAt;

    /**
     * @var ReturnChange|null
     */
    protected $changes;

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param string|null $id
     * @return ReturnChangeShell
     */
    public function setId(?string $id): ReturnChangeShell
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getModificationState(): ?string
    {
        return $this->modificationState;
    }

    /**
     * @param string|null $modificationState
     * @return ReturnChangeShell
     */
    public function setModificationState(?string $modificationState): ReturnChangeShell
    {
        $this->modificationState = $modificationState;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getModifiedFrom(): ?string
    {
        return $this->modifiedFrom;
    }

    /**
     * @param string|null $modifiedFrom
     * @return ReturnChangeShell
     */
    public function setModifiedFrom(?string $modifiedFrom): ReturnChangeShell
    {
        $this->modifiedFrom = $modifiedFrom;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getUpdatedAt(): ?DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param DateTime|string|null $updatedAt
     * @return ReturnChangeShell
     * @throws Exception
     */
    public function setUpdatedAt($updatedAt): ReturnChangeShell
    {
        if ($updatedAt === null) {
            return $this;
        }

        if (is_string($updatedAt)) {
            $updatedAt = (new DateTime($updatedAt))->setTimezone(new DateTimeZone('UTC'));
        }

        $this->checkDate($updatedAt);
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return ReturnChange|null
     */
    public function getChanges(): ?ReturnChange
    {
        return $this->changes;
    }

    /**
     * @param ReturnChange $changes
     * @return ReturnChangeShell
     */
    public function setChanges(ReturnChange $changes): ReturnChangeShell
    {
        $this->changes = $changes;
        return $this;
    }

    /**
     * @param ReturnChange $change
     * @param string|null $key
     * @return ReturnChangeShell
     */
    public function addChange(ReturnChange $change, string $key = null): ReturnChangeShell
    {
        if ($key === null) {
            $this->changes[] = $change;
        } else {
            $this->changes[$key] = $change;
        }

        return $this;
    }

    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('id', 'string', null),
            new PropertyInfo('modificationState', 'string', null),
            new PropertyInfo('modifiedFrom', 'string', null),
            new PropertyInfo('updatedAt', DateTime::class, null),
            new PropertyInfo('changes', ReturnChange::class, null, true),
        ]);
    }
}
