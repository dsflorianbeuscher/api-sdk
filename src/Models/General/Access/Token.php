<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General\Access;

use DateTime;
use DateTimeZone;
use Exception;
use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;

/**
 * Class Token
 * @package Jtl\Fulfillment\Api\Sdk\Models\General
 */
class Token extends DataModel
{
    /**
     * @var string|null
     */
    protected $tokenId;
    
    /**
     * @var string|null
     */
    protected $token;
    
    /**
     * @var string|null
     */
    protected $name;
    
    /**
     * @var string|null
     */
    protected $applicationId;
    
    /**
     * @var string|null
     */
    protected $clientId;
    
    /**
     * @var DateTime|null
     */
    protected $createdAt;
    
    /**
     * @var string[]
     */
    protected $scopes = [];
    
    /**
     * @return string|null
     */
    public function getTokenId(): ?string
    {
        return $this->tokenId;
    }
    
    /**
     * @param string|null $tokenId
     * @return Token
     */
    public function setTokenId(?string $tokenId): self
    {
        $this->tokenId = $tokenId;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getToken(): ?string
    {
        return $this->token;
    }
    
    /**
     * @param string|null $token
     * @return Token
     */
    public function setToken(?string $token): self
    {
        $this->token = $token;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }
    
    /**
     * @param string|null $name
     * @return Token
     */
    public function setName(?string $name): self
    {
        $this->name = $name;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getApplicationId(): ?string
    {
        return $this->applicationId;
    }
    
    /**
     * @param string|null $applicationId
     * @return Token
     */
    public function setApplicationId(?string $applicationId): self
    {
        $this->applicationId = $applicationId;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getClientId(): ?string
    {
        return $this->clientId;
    }
    
    /**
     * @param string|null $clientId
     * @return Token
     */
    public function setClientId(?string $clientId): self
    {
        $this->clientId = $clientId;
        
        return $this;
    }
    
    /**
     * @return DateTime|null
     */
    public function getCreatedAt(): ?DateTime
    {
        return $this->createdAt;
    }
    
    /**
     * @param DateTime|string|null $createdAt
     * @return Token
     * @throws Exception
     */
    public function setCreatedAt($createdAt): self
    {
        if ($createdAt === null) {
            return $this;
        }
        
        if (is_string($createdAt)) {
            $createdAt = (new DateTime($createdAt))->setTimezone(new DateTimeZone('UTC'));
        }
        
        $this->checkDate($createdAt);
        $this->createdAt = $createdAt;
        
        return $this;
    }
    
    /**
     * @return string[]
     */
    public function getScopes(): array
    {
        return $this->scopes;
    }
    
    /**
     * @param string[] $scopes
     * @return Token
     */
    public function setScopes(array $scopes): self
    {
        $this->scopes = $scopes;
        
        return $this;
    }
    
    /**
     * @param string $scope
     * @param string|null $key
     * @return Token
     */
    public function addScope(string $scope, string $key = null): self
    {
        if ($key === null) {
            $this->scopes[] = $scope;
        } else {
            $this->scopes[$key] = $scope;
        }
        
        return $this;
    }
    
    /**
     * @inheritDoc
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('tokenId'),
            new PropertyInfo('token'),
            new PropertyInfo('name'),
            new PropertyInfo('applicationId'),
            new PropertyInfo('clientId'),
            new PropertyInfo('createdAt', DateTime::class, null),
            new PropertyInfo('scopes', 'string', [], false, true),
        ]);
    }
}
