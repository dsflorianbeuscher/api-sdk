<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General;

use DateTime;
use DateTimeZone;
use Exception;
use function is_string;
use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;

/**
 * Class ModificationInfo
 * @package Jtl\Fulfillment\Api\Sdk\Models\General
 */
class ModificationInfo extends DataModel
{
    /**
     * @var DateTime|null
     */
    protected $createdAt;
    
    /**
     * @var DateTime|null
     */
    protected $updatedAt;
    
    /**
     * @var DateTime|null
     */
    protected $deletedAt;
    
    /**
     * @var string|null
     */
    protected $state;
    
    /**
     * @return DateTime|null
     */
    public function getCreatedAt(): ?DateTime
    {
        return $this->createdAt;
    }
    
    /**
     * @param DateTime|string|null $createdAt
     * @return ModificationInfo
     * @throws Exception
     */
    public function setCreatedAt($createdAt): ModificationInfo
    {
        if ($createdAt === null) {
            return $this;
        }
        
        if (is_string($createdAt)) {
            $createdAt = (new DateTime($createdAt))->setTimezone(new DateTimeZone('UTC'));
        }
        
        $this->checkDate($createdAt);
        $this->createdAt = $createdAt;
        
        return $this;
    }
    
    /**
     * @return DateTime|null
     */
    public function getUpdatedAt(): ?DateTime
    {
        return $this->updatedAt;
    }
    
    /**
     * @param DateTime|string|null $updatedAt
     * @return ModificationInfo
     * @throws Exception
     */
    public function setUpdatedAt($updatedAt): ModificationInfo
    {
        if ($updatedAt === null) {
            return $this;
        }
        
        if (is_string($updatedAt)) {
            $updatedAt = (new DateTime($updatedAt))->setTimezone(new DateTimeZone('UTC'));
        }
    
        $this->checkDate($updatedAt);
        $this->updatedAt = $updatedAt;
        
        return $this;
    }
    
    /**
     * @return DateTime|null
     */
    public function getDeletedAt(): ?DateTime
    {
        return $this->deletedAt;
    }
    
    /**
     * @param DateTime|string $deletedAt
     * @return ModificationInfo
     * @throws Exception
     */
    public function setDeletedAt($deletedAt): ModificationInfo
    {
        if ($deletedAt === null) {
            return $this;
        }
        
        if (is_string($deletedAt)) {
            $deletedAt = (new DateTime($deletedAt))->setTimezone(new DateTimeZone('UTC'));
        }
    
        $this->checkDate($deletedAt);
        $this->deletedAt = $deletedAt;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getState(): ?string
    {
        return $this->state;
    }
    
    /**
     * @param string|null $state
     * @return ModificationInfo
     */
    public function setState(?string $state): ModificationInfo
    {
        $this->state = $state;
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return (new PropertyCollection())->setProperties([
            new PropertyInfo('createdAt', DateTime::class, null),
            new PropertyInfo('updatedAt', DateTime::class, null),
            new PropertyInfo('deletedAt', DateTime::class, null),
            new PropertyInfo('state', 'string', null)
        ]);
    }
}
