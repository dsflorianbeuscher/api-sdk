<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General\Inbound;

use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;

/**
 * Class InboundShippingNotificationItem
 * @package Jtl\Fulfillment\Api\Sdk\Models\General\Inbound
 */
class InboundShippingNotificationItem extends DataModel
{
    /**
     * @var string|null
     */
    protected $inboundShippingNotificationItemId;
    
    /**
     * @var string|null
     */
    protected $jfsku;
    
    /**
     * @var float|null
     */
    protected $quantity;
    
    /**
     * @var string|null
     */
    protected $note;
    
    /**
     * @return string|null
     */
    public function getInboundShippingNotificationItemId(): ?string
    {
        return $this->inboundShippingNotificationItemId;
    }
    
    /**
     * @param string|null $inboundShippingNotificationItemId
     * @return InboundShippingNotificationItem
     */
    public function setInboundShippingNotificationItemId(
        ?string $inboundShippingNotificationItemId
    ): InboundShippingNotificationItem {
        $this->inboundShippingNotificationItemId = $inboundShippingNotificationItemId;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getJfsku(): ?string
    {
        return $this->jfsku;
    }
    
    /**
     * @param string|null $jfsku
     * @return InboundShippingNotificationItem
     */
    public function setJfsku(?string $jfsku): InboundShippingNotificationItem
    {
        $this->jfsku = $jfsku;
        
        return $this;
    }
    
    /**
     * @return float|null
     */
    public function getQuantity(): ?float
    {
        return $this->quantity;
    }
    
    /**
     * @param float|null $quantity
     * @return InboundShippingNotificationItem
     */
    public function setQuantity(?float $quantity): InboundShippingNotificationItem
    {
        $this->quantity = $quantity;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getNote(): ?string
    {
        return $this->note;
    }
    
    /**
     * @param string|null $note
     * @return InboundShippingNotificationItem
     */
    public function setNote(?string $note): InboundShippingNotificationItem
    {
        $this->note = $note;
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('inboundShippingNotificationItemId'),
            new PropertyInfo('jfsku', 'string', null),
            new PropertyInfo('quantity', 'float', null),
            new PropertyInfo('note', 'string', null)
        ]);
    }
}
