<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General\Inbound;

use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;

/**
 * Class InboundItem
 * @package Jtl\Fulfillment\Api\Sdk\Models\General\Inbound
 */
class InboundItem extends DataModel
{
    /**
     * @var float|null
     */
    protected $quantityOpen;
    
    /**
     * @var string|null
     */
    protected $inboundItemId;
    
    /**
     * @var string|null
     */
    protected $jfsku;
    
    /**
     * @var float|null
     */
    protected $quantity;
    
    /**
     * @var string|null
     */
    protected $supplierSku;
    
    /**
     * @var string|null
     */
    protected $merchantSku;
    
    /**
     * @var string|null
     */
    protected $supplierProductName;
    
    /**
     * @var string|null
     */
    protected $note;
    
    /**
     * @return float|null
     */
    public function getQuantityOpen(): ?float
    {
        return $this->quantityOpen;
    }
    
    /**
     * @param float|null $quantityOpen
     * @return InboundItem
     */
    public function setQuantityOpen(?float $quantityOpen): InboundItem
    {
        $this->quantityOpen = $quantityOpen;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getInboundItemId(): ?string
    {
        return $this->inboundItemId;
    }
    
    /**
     * @param string|null $inboundItemId
     * @return InboundItem
     */
    public function setInboundItemId(?string $inboundItemId): InboundItem
    {
        $this->inboundItemId = $inboundItemId;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getJfsku(): ?string
    {
        return $this->jfsku;
    }
    
    /**
     * @param string|null $jfsku
     * @return InboundItem
     */
    public function setJfsku(?string $jfsku): InboundItem
    {
        $this->jfsku = $jfsku;
        
        return $this;
    }
    
    /**
     * @return float|null
     */
    public function getQuantity(): ?float
    {
        return $this->quantity;
    }
    
    /**
     * @param float|null $quantity
     * @return InboundItem
     */
    public function setQuantity(?float $quantity): InboundItem
    {
        $this->quantity = $quantity;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getSupplierSku(): ?string
    {
        return $this->supplierSku;
    }
    
    /**
     * @param string|null $supplierSku
     * @return InboundItem
     */
    public function setSupplierSku(?string $supplierSku): InboundItem
    {
        $this->supplierSku = $supplierSku;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getMerchantSku(): ?string
    {
        return $this->merchantSku;
    }
    
    /**
     * @param string|null $merchantSku
     * @return InboundItem
     */
    public function setMerchantSku(?string $merchantSku): InboundItem
    {
        $this->merchantSku = $merchantSku;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getSupplierProductName(): ?string
    {
        return $this->supplierProductName;
    }
    
    /**
     * @param string|null $supplierProductName
     * @return InboundItem
     */
    public function setSupplierProductName(?string $supplierProductName): InboundItem
    {
        $this->supplierProductName = $supplierProductName;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getNote(): ?string
    {
        return $this->note;
    }
    
    /**
     * @param string|null $note
     * @return InboundItem
     */
    public function setNote(?string $note): InboundItem
    {
        $this->note = $note;
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('quantityOpen', 'float', null),
            new PropertyInfo('inboundItemId', 'string', null),
            new PropertyInfo('jfsku', 'string', null),
            new PropertyInfo('quantity', 'float', null),
            new PropertyInfo('supplierSku', 'string', null),
            new PropertyInfo('merchantSku', 'string', null),
            new PropertyInfo('supplierProductName', 'string', null),
            new PropertyInfo('note', 'string', null)
        ]);
    }
}
