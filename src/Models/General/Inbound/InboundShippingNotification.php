<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General\Inbound;

use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\General\Package\Package;

/**
 * Class InboundShippingNotification
 * @package Jtl\Fulfillment\Api\Sdk\Models\General\Inbound
 */
class InboundShippingNotification extends DataModel
{
    /**
     * @var string|null
     */
    protected $inboundId;
    
    /**
     * @var string|null
     */
    protected $inboundShippingNotificationId;

    /**
     * @var InboundShippingNotificationItem[]
     */
    protected $items = [];
    
    /**
     * @var Package[]
     */
    protected $packages = [];
    
    /**
     * @var string|null
     */
    protected $merchantShippingNotificationNumber;
    
    /**
     * @var string|null
     */
    protected $note;
    
    /**
     * @return string|null
     */
    public function getInboundId(): ?string
    {
        return $this->inboundId;
    }
    
    /**
     * @param string|null $inboundId
     * @return InboundShippingNotification
     */
    public function setInboundId(?string $inboundId): InboundShippingNotification
    {
        $this->inboundId = $inboundId;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getInboundShippingNotificationId(): ?string
    {
        return $this->inboundShippingNotificationId;
    }
    
    /**
     * @param string|null $inboundShippingNotificationId
     * @return InboundShippingNotification
     */
    public function setInboundShippingNotificationId(
        ?string $inboundShippingNotificationId
    ): InboundShippingNotification {
        $this->inboundShippingNotificationId = $inboundShippingNotificationId;
        
        return $this;
    }
    
    /**
     * @return InboundShippingNotificationItem[]
     */
    public function getItems(): array
    {
        return $this->items;
    }
    
    /**
     * @param InboundShippingNotificationItem[] $items
     * @return InboundShippingNotification
     */
    public function setItems(array $items): InboundShippingNotification
    {
        $this->items = $items;
        
        return $this;
    }
    
    /**
     * @param InboundShippingNotificationItem $item
     * @param string|null $key
     * @return InboundShippingNotification
     */
    public function addItem(InboundShippingNotificationItem $item, string $key = null): InboundShippingNotification
    {
        if ($key === null) {
            $this->items[] = $item;
        } else {
            $this->items[$key] = $item;
        }
        
        return $this;
    }
    
    /**
     * @return Package[]
     */
    public function getPackages(): array
    {
        return $this->packages;
    }
    
    /**
     * @param Package[] $packages
     * @return InboundShippingNotification
     */
    public function setPackages(array $packages): InboundShippingNotification
    {
        $this->packages = $packages;
        
        return $this;
    }
    
    /**
     * @param Package $package
     * @param string|null $key
     * @return InboundShippingNotification
     */
    public function addPackage(Package $package, string $key = null): InboundShippingNotification
    {
        if ($key === null) {
            $this->packages[] = $package;
        } else {
            $this->packages[$key] = $package;
        }
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getMerchantShippingNotificationNumber(): ?string
    {
        return $this->merchantShippingNotificationNumber;
    }
    
    /**
     * @param string|null $merchantShippingNotificationNumber
     * @return InboundShippingNotification
     */
    public function setMerchantShippingNotificationNumber(
        ?string $merchantShippingNotificationNumber
    ): InboundShippingNotification {
        $this->merchantShippingNotificationNumber = $merchantShippingNotificationNumber;
        
        return $this;
    }
    
    /**
     * @return string|null
     */
    public function getNote(): ?string
    {
        return $this->note;
    }
    
    /**
     * @param string|null $note
     * @return InboundShippingNotification
     */
    public function setNote(?string $note): InboundShippingNotification
    {
        $this->note = $note;
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('inboundId', 'string', null),
            new PropertyInfo('inboundShippingNotificationId', 'string', null),
            new PropertyInfo('items', InboundShippingNotificationItem::class, [], true, true),
            new PropertyInfo('packages', Package::class, [], true, true),
            new PropertyInfo('merchantShippingNotificationNumber', 'string', null),
            new PropertyInfo('note', 'string', null)
        ]);
    }
}
