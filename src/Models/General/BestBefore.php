<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General;

use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;

/**
 * Class BestBefore
 * @package Jtl\Fulfillment\Api\Sdk\Models\General
 */
class BestBefore extends DataModel
{
    /**
     * @var int|null
     */
    protected $year;
    
    /**
     * @var int|null
     */
    protected $month;
    
    /**
     * @var int|null
     */
    protected $day;
    
    /**
     * @return int|null
     */
    public function getYear(): ?int
    {
        return $this->year;
    }
    
    /**
     * @param int|null $year
     * @return BestBefore
     */
    public function setYear(?int $year): BestBefore
    {
        $this->year = $year;
        
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getMonth(): ?int
    {
        return $this->month;
    }
    
    /**
     * @param int|null $month
     * @return BestBefore
     */
    public function setMonth(?int $month): BestBefore
    {
        $this->month = $month;
        
        return $this;
    }
    
    /**
     * @return int|null
     */
    public function getDay(): ?int
    {
        return $this->day;
    }
    
    /**
     * @param int|null $day
     * @return BestBefore
     */
    public function setDay(?int $day): BestBefore
    {
        $this->day = $day;
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('year', 'int', null),
            new PropertyInfo('month', 'int', null),
            new PropertyInfo('day', 'int', null)
        ]);
    }
}
