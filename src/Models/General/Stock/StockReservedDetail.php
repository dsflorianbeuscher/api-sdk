<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General\Stock;

use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;

/**
 * Class StockReservedDetail
 * @package Jtl\Fulfillment\Api\Sdk\Models\General\Stock
 */
class StockReservedDetail extends DataModel
{
    /**
     * @var string|null
     */
    protected $outboundId;

    /**
     * @var string|null
     */
    protected $outboundItemId;

    /**
     * @var float|null
     */
    protected $quantityReserved;

    /**
     * @return string|null
     */
    public function getOutboundId(): ?string
    {
        return $this->outboundId;
    }

    /**
     * @param string|null $outboundId
     * @return StockReservedDetail
     */
    public function setOutboundId(?string $outboundId): StockReservedDetail
    {
        $this->outboundId = $outboundId;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getOutboundItemId(): ?string
    {
        return $this->outboundItemId;
    }

    /**
     * @param string|null $outboundItemId
     * @return StockReservedDetail
     */
    public function setOutboundItemId(?string $outboundItemId): StockReservedDetail
    {
        $this->outboundItemId = $outboundItemId;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getQuantityReserved(): ?float
    {
        return $this->quantityReserved;
    }

    /**
     * @param float|null $quantityReserved
     * @return StockReservedDetail
     */
    public function setQuantityReserved(?float $quantityReserved): StockReservedDetail
    {
        $this->quantityReserved = $quantityReserved;

        return $this;
    }

    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('outboundId', 'string', null),
            new PropertyInfo('outboundItemId', 'string', null),
            new PropertyInfo('quantityReserved', 'float', null)
        ]);
    }
}
