<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General\Stock;

use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;

class StockInboundItem extends DataModel
{
    /**
     * @var string|null
     */
    protected $inboundItemId;
    
    /**
     * @return string|null
     */
    public function getInboundItemId(): ?string
    {
        return $this->inboundItemId;
    }
    
    /**
     * @param string|null $inboundItemId
     * @return StockInboundItem
     */
    public function setInboundItemId(?string $inboundItemId): StockInboundItem
    {
        $this->inboundItemId = $inboundItemId;
        
        return $this;
    }
    
    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('inboundItemId', 'string', null)
        ]);
    }
}
