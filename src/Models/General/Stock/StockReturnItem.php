<?php
namespace Jtl\Fulfillment\Api\Sdk\Models\General\Stock;

use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Izzle\Model\PropertyCollection;
use Izzle\Model\PropertyInfo;

class StockReturnItem extends DataModel
{
    /**
     * @var string|null
     */
    protected $returnId;

    /**
     * @var string|null
     */
    protected $returnItemId;

    /**
     * @return string|null
     */
    public function getReturnId(): ?string
    {
        return $this->returnId;
    }

    /**
     * @param string|null $returnId
     * @return StockReturnItem
     */
    public function setReturnId(?string $returnId): StockReturnItem
    {
        $this->returnId = $returnId;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getReturnItemId(): ?string
    {
        return $this->returnItemId;
    }

    /**
     * @param string|null $returnItemId
     * @return StockReturnItem
     */
    public function setReturnItemId(?string $returnItemId): StockReturnItem
    {
        $this->returnItemId = $returnItemId;
        return $this;
    }

    /**
     * @return PropertyCollection
     */
    protected function loadProperties(): PropertyCollection
    {
        return new PropertyCollection([
            new PropertyInfo('returnId', 'string', null),
            new PropertyInfo('returnItemId', 'string', null),
        ]);
    }
}
