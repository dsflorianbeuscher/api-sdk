<?php
namespace Jtl\Fulfillment\Api\Sdk\Query;

use Closure;
use Illuminate\Contracts\Support\Arrayable;
use InvalidArgumentException;

/**
 * Class Builder
 * @package Jtl\Fulfillment\Api\Sdk\Query
 */
class Builder
{
    public const OPERATOR_NOT_EQUAL = '!=';
    public const OPERATOR_EQUAL = '=';
    public const OPERATOR_GREATER_EQUAL = '>=';
    public const OPERATOR_LESS_EQUAL = '<=';
    public const OPERATOR_GREATER_THAN = '>';
    public const OPERATOR_LESS_THAN = '<';
    
    /**
     * All of the available clause operators.
     *
     * @var string[]
     */
    protected $operators = [
        self::OPERATOR_NOT_EQUAL,
        self::OPERATOR_EQUAL,
        self::OPERATOR_GREATER_EQUAL,
        self::OPERATOR_LESS_EQUAL,
        self::OPERATOR_GREATER_THAN,
        self::OPERATOR_LESS_THAN
    ];
    
    /**
     * The where constraints for the query.
     *
     * @var array
     */
    public $wheres = [];
    
    /**
     * @var array
     */
    public $expands = [];
    
    /**
     * Add an "or where" clause to the query.
     *
     * @param string|array|Closure $column
     * @param string|null $operator
     * @param mixed|null $value
     * @return Builder
     */
    public function orWhere($column, string $operator = null, $value = null): self
    {
        [$value, $operator] = $this->prepareValueAndOperator(
            $value,
            $operator,
            func_num_args() === 2
        );
        
        return $this->where($column, $operator, $value, 'or');
    }
    
    /**
     * Add a basic where clause to the query.
     *
     * @param string|array|Closure $column
     * @param string|null $operator
     * @param mixed|null $value
     * @param string $boolean
     * @return Builder
     */
    public function where($column, string $operator = null, $value = null, string $boolean = 'and'): self
    {
        // If the column is an array, we will assume it is an array of key-value pairs
        // and can add them each as a where clause. We will maintain the boolean we
        // received when the method was called and pass it into the nested where.
        if (is_array($column)) {
            return $this->addArrayOfWheres($column, $boolean);
        }
        
        // Here we will make some assumptions about the operator. If only 2 values are
        // passed to the method, we will assume that the operator is an equals sign
        // and keep going. Otherwise, we'll require the operator to be passed in.
        [$value, $operator] = $this->prepareValueAndOperator(
            $value,
            $operator,
            func_num_args() === 2
        );
        
        // If the columns is actually a Closure instance, we will assume the developer
        // wants to begin a nested where statement which is wrapped in parenthesis.
        // We'll add that Closure to the query then return back out immediately.
        if ($column instanceof Closure) {
            return $this->whereNested($column, $boolean);
        }
        
        // If the given operator is not found in the list of valid operators we will
        // assume that the developer is just short-cutting the '=' operators and
        // we will set the operators to '=' and set the values appropriately.
        if ($this->invalidOperator($operator)) {
            [$value, $operator] = [$operator, self::OPERATOR_EQUAL];
        }
        
        // If the value is "null", we will just assume the developer wants to add a
        // where null clause to the query. So, we will allow a short-cut here to
        // that method for convenience so the developer doesn't have to check.
        if ($value === null) {
            return $this->whereNull($column, $boolean, $operator !== self::OPERATOR_EQUAL);
        }
        
        $type = 'Basic';
        
        // Now that we are working with just a simple query we can put the elements
        // in our array and add the query binding to our array of bindings that
        // will be bound to each SQL statements when it is finally executed.
        $this->wheres[] = compact(
            'type',
            'column',
            'operator',
            'value',
            'boolean'
        );
        
        return $this;
    }
    
    /**
     * Add a nested where statement to the query.
     *
     * @param Closure $callback
     * @param string $boolean
     * @return Builder
     */
    public function whereNested(Closure $callback, $boolean = 'and'): self
    {
        $callback($query = $this->forNestedWhere());
        
        return $this->addNestedWhereQuery($query, $boolean);
    }
    
    /**
     * Create a new query instance for nested where condition.
     *
     * @return Builder
     */
    public function forNestedWhere(): Builder
    {
        return $this->newQuery();
    }
    
    /**
     * Add another query builder as a nested where to the query builder.
     *
     * @param Builder $query
     * @param string $boolean
     * @return Builder
     */
    public function addNestedWhereQuery(Builder $query, $boolean = 'and'): self
    {
        if (count($query->wheres)) {
            $type = 'Nested';
            
            $this->wheres[] = compact('type', 'query', 'boolean');
        }
        
        return $this;
    }
    
    /**
     * @param string $child
     * @param string $column
     * @param string $operator
     * @param mixed|null $value
     * @return Builder
     */
    public function orWhereSub(string $child, string $column, string $operator = null, $value = null): Builder
    {
        return $this->whereSub($column, $value, 'or');
    }
    
    /**
     * @param string $child
     * @param string $column
     * @param string|null $operator
     * @param mixed|null $value
     * @param string $boolean
     * @return Builder
     */
    public function whereSub(string $child, string $column = null, string $operator = null, $value = null, string $boolean = 'and'): self
    {
        $type = 'sub';
    
        $this->wheres[] = compact('child', 'type', 'column', 'operator', 'value', 'boolean');
    
        return $this;
    }
    
    /**
     * Add a "where null" clause to the query.
     *
     * @param string $column
     * @param string $boolean
     * @param bool $not
     * @return Builder
     */
    public function whereNull($column, $boolean = 'and', $not = false): Builder
    {
        $type = $not ? 'NotNull' : 'Null';
        
        $this->wheres[] = compact('type', 'column', 'boolean');
        
        return $this;
    }
    
    /**
     *  Add a "contains" clause to the query
     *
     * @param $column
     * @param mixed|null $value
     * @param string $boolean
     * @return $this
     */
    public function whereContains($column, $value = null, string $boolean = 'and'): Builder
    {
        // If the column is an array, we will assume it is an array of key-value pairs
        // and can add them each as a where clause. We will maintain the boolean we
        // received when the method was called and pass it into the nested where.
        if (is_array($column)) {
            return $this->addArrayOfWheres($column, $boolean, 'whereContains');
        }
        
        $type = 'contains';
    
        $this->wheres[] = compact('type', 'column', 'value', 'boolean');
    
        return $this;
    }
    
    /**
     * @param $column
     * @param mixed|null $value
     * @return Builder
     */
    public function orWhereContains($column, $value = null): Builder
    {
        return $this->whereContains($column, $value, 'or');
    }
    
    /**
     *  Add a "startswith" clause to the query
     *
     * @param $column
     * @param mixed|null $value
     * @param string $boolean
     * @return $this
     */
    public function whereStartsWith($column, $value = null, string $boolean = 'and'): Builder
    {
        $type = 'startswith';
        
        $this->wheres[] = compact('type', 'column', 'value', 'boolean');
        
        return $this;
    }
    
    /**
     * @param $column
     * @param mixed|null $value
     * @return Builder
     */
    public function orWhereStartsWith($column, $value = null): Builder
    {
        return $this->whereStartsWith($column, $value, 'or');
    }
    
    /**
     *  Add a "endswith" clause to the query
     *
     * @param $column
     * @param mixed|null $value
     * @param string $boolean
     * @return $this
     */
    public function whereEndsWith($column, $value = null, string $boolean = 'and'): Builder
    {
        $type = 'endswith';
        
        $this->wheres[] = compact('type', 'column', 'value', 'boolean');
        
        return $this;
    }
    
    /**
     * @param $column
     * @param mixed|null $value
     * @return Builder
     */
    public function orWhereEndsWith($column, $value = null): Builder
    {
        return $this->whereEndsWith($column, $value, 'or');
    }
    
    /**
     * @param mixed $column
     * @param array $values
     * @param string $boolean
     * @param bool $not
     * @return Builder
     */
    public function whereIn($column, array $values, $boolean = 'and', $not = false): Builder
    {
        $type = $not ? 'NotIn' : 'In';
        
        // Next, if the value is Arrayable we need to cast it to its raw array form so we
        // have the underlying array value instead of an Arrayable object which is not
        // able to be added as a binding, etc. We will then add to the wheres array.
        if ($values instanceof Arrayable) {
            $values = $values->toArray();
        }
        
        $this->wheres[] = compact('type', 'column', 'values', 'boolean');
        
        return $this;
    }
    
    /**
     * Add an "or where in" clause to the query.
     *
     * @param mixed $column
     * @param array $values
     * @return Builder|static
     */
    public function orWhereIn($column, array $values): Builder
    {
        return $this->whereIn($column, $values, 'or');
    }
    
    /**
     * @return Builder
     */
    public function clean(): Builder
    {
        $this->wheres = [];
        $this->expands = [];
        
        return $this;
    }
    
    /**
     * @param string|array $expand
     * @throws InvalidArgumentException
     * @return Builder
     */
    public function with($expand): Builder
    {
        if (is_array($expand)) {
            $this->expands = array_merge($this->expands, $expand);
            
            return $this;
        }
        
        if (!is_string($expand)) {
            throw new InvalidArgumentException('Param expand must be type of string or array');
        }
        
        if (!in_array($expand, $this->expands, true)) {
            $this->expands[] = $expand;
        }
    
        return $this;
    }
    
    /**
     * Add an array of where clauses to the query.
     *
     * @param  array  $column
     * @param  string  $boolean
     * @param  string  $method
     * @return $this
     */
    protected function addArrayOfWheres($column, $boolean, $method = 'where'): Builder
    {
        return $this->whereNested(static function ($query) use ($column, $method, $boolean) {
            foreach ($column as $key => $value) {
                if (is_numeric($key) && is_array($value)) {
                    if (count($value) === 2) {
                        $value[] = $boolean;
                    }
                    
                    $query->{$method}(...array_values($value));
                } else {
                    $query->$method($key, Builder::OPERATOR_EQUAL, $value, $boolean);
                }
            }
        }, $boolean);
    }
    
    /**
     * Get a new instance of the query builder.
     *
     * @return Builder
     */
    protected function newQuery(): self
    {
        return new static();
    }
    
    /**
     * Determine if the given operator is supported.
     *
     * @param string $operator
     * @return bool
     */
    protected function invalidOperator($operator): bool
    {
        return !in_array(strtolower($operator), $this->operators, true);
    }
    
    /**
     * Prepare the value and operator for a where clause.
     *
     * @param mixed $value
     * @param string $operator
     * @param bool $useDefault
     * @return array
     * @throws InvalidArgumentException
     */
    protected function prepareValueAndOperator($value, $operator, $useDefault = false): array
    {
        if ($useDefault) {
            return [$operator, self::OPERATOR_EQUAL];
        }
        
        if ($this->invalidOperatorAndValue($operator, $value)) {
            throw new InvalidArgumentException('Illegal operator and value combination.');
        }
        
        return [$value, $operator];
    }
    
    /**
     * Determine if the given operator and value combination is legal.
     *
     * Prevents using Null values with invalid operators.
     *
     * @param string $operator
     * @param mixed $value
     * @return bool
     */
    protected function invalidOperatorAndValue($operator, $value): bool
    {
        return $value === null && in_array($operator, $this->operators, true) &&
            !in_array($operator, [self::OPERATOR_EQUAL, self::OPERATOR_NOT_EQUAL], true);
    }
}
