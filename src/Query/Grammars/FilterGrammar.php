<?php
namespace Jtl\Fulfillment\Api\Sdk\Query\Grammars;

use Jtl\Fulfillment\Api\Sdk\Query\Builder;

class FilterGrammar implements GrammarInterface
{
    /**
     * @param Builder $query
     * @return string
     */
    public function compileWheres(Builder $query): string
    {
        if ($query->wheres === null) {
            return '';
        }
        
        if (count($sql = $this->internCompileWheresToArray($query)) > 0) {
            return $this->concatenateWhereClauses($query, $sql);
        }
        
        return '';
    }
    
    public function compileWheresToArray(Builder $query, array &$data = [], int $index = 0): array
    {
        foreach ($query->wheres as $where) {
            if ($where['type'] === 'Nested') {
                $this->compileWheresToArray($where['query'], $data);
                continue;
            }

            if ($where['boolean'] === 'or') {
                $index++;
            }
            
            $data[$index][] = $this->whereBasic($query, $where);
        }

        return array_values(array_map(function ($ands) {
            return $this->removeLeadingBoolean(implode(',', $ands));
        }, $data));
    }
    
    /**
     * Get an array of all the where clauses for the query.
     *
     * @param Builder $query
     * @return array
     */
    protected function internCompileWheresToArray(Builder $query): array
    {
        return array_map(function ($where) use ($query) {
            return $this->translateBoolean($where['boolean']) . $this->{"where{$where['type']}"}($query, $where);
        }, $query->wheres);
    }
    
    /**
     * @param string $boolean
     * @return string
     */
    protected function translateBoolean(string $boolean): string
    {
        return $boolean === 'and' ? ',' : '&filter[]=';
    }
    
    /**
     * Format the where clause statements into one string.
     *
     * @param Builder $query
     * @param array $sql
     * @return string
     */
    protected function concatenateWhereClauses(Builder $query, array $sql): string
    {
        return sprintf('filter[]=%s', $this->removeLeadingBoolean(implode('', $sql)));
    }
    
    /**
     * Remove the leading boolean from a statement.
     *
     * @param string $value
     * @return string
     */
    protected function removeLeadingBoolean(string $value): string
    {
        return preg_replace('/,|filter\[\]= /i', '', $value, 1);
    }
    
    /**
     * Compile a basic where clause.
     *
     * @param Builder $query
     * @param array $where
     * @return string
     */
    protected function whereBasic(Builder $query, array $where): string
    {
        return $where['column'] . $where['operator'] . $where['value'];
    }
    
    /**
     * Compile a nested where clause.
     *
     * @param Builder $query
     * @param array $where
     * @return string
     */
    protected function whereNested(Builder $query, array $where): string
    {
        return substr($this->compileWheres($where['query']), 9);
    }
    
    /**
     * Compile a "where null" clause.
     *
     * @param Builder $query
     * @param array $where
     * @return string
     */
    protected function whereNull(Builder $query, array $where): string
    {
        return sprintf('%s is null', $where['column']);
    }
}
