<?php
namespace Jtl\Fulfillment\Api\Sdk\Query\Grammars;

use Jtl\Fulfillment\Api\Sdk\Query\Builder;

interface GrammarInterface
{
    /**
     * @param Builder $query
     * @return string
     */
    public function compileWheres(Builder $query): string;
    
    /**
     * @param Builder $query
     * @param array $data
     * @param int $index
     * @return array
     */
    public function compileWheresToArray(Builder $query, array &$data = [], int $index = 0): array;
}
