<?php
namespace Jtl\Fulfillment\Api\Sdk;

use InvalidArgumentException;
use Izzle\Model\Model;
use Jtl\Fulfillment\Api\Sdk\Cache\NullCache;
use Jtl\Fulfillment\Api\Sdk\Models\Query;
use Psr\SimpleCache\CacheInterface;
use ReflectionException;
use ReflectionMethod;
use RuntimeException;

/**
 * Class Client
 * @package Jtl\Fulfillment\Api\Sdk
 */
class Client
{
    /**
     * @var String
     */
    private $userId;
    
    /**
     * @var HttpInterface
     */
    private $http;

    /**
     * @var CacheInterface
     */
    private $cache;
    
    /**
     * Client constructor.
     * @param HttpInterface|\GuzzleHttp\Client $http - or ClientInterface from Guzzle
     * @param string $userId - Unique Context for Caching
     * @throws ReflectionException
     * @throws InvalidArgumentException
     */
    public function __construct($http, string $userId = '')
    {
        $this->setHttp($http);
        
        $this->userId = $userId;
        
        Model::$serializeWithSnakeKeys = false;
        Query::$oDataSerialization = true;
    }
    
    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }
    
    /**
     * @return HttpInterface
     * @throws RuntimeException
     */
    public function getHttp()
    {
        if ($this->http === null) {
            throw new RuntimeException('Missing Http Client');
        }

        return $this->http;
    }
    
    /**
     * @param HttpInterface $http
     * @return self
     * @throws ReflectionException
     * @throws InvalidArgumentException
     */
    public function setHttp($http): self
    {
        $this->checkHttpClient($http);

        $this->http = $http;

        return $this;
    }

    /**
     * @return CacheInterface
     */
    public function getCache(): CacheInterface
    {
        if ($this->cache === null) {
            $this->cache = $this->createDefaultCache();
        }

        return $this->cache;
    }

    /**
     * @param CacheInterface $cache
     * @return self
     */
    public function setCache(CacheInterface $cache): self
    {
        $this->cache = $cache;

        return $this;
    }

    /**
     * @return NullCache
     */
    protected function createDefaultCache(): NullCache
    {
        return new NullCache();
    }
    
    /**
     * @param object $http
     * @return bool
     * @throws InvalidArgumentException
     * @throws ReflectionException
     */
    protected function checkHttpClient($http): bool
    {
        $method = new ReflectionMethod($http, 'request');
        $parameters = $method->getParameters();
        if (count($parameters) === 3 &&
            $parameters[0]->getName() === 'method' &&
            $parameters[1]->getName() === 'uri' &&
            $parameters[2]->getName() === 'options') {
            return true;
        }
        
        throw new InvalidArgumentException('Parameter $http must be a class with method request(string $method, string $uri, array $options = [])');
    }
}
