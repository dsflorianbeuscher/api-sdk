<?php
namespace Jtl\Fulfillment\Api\Sdk\Resources\General;

use Throwable;
use Jtl\Fulfillment\Api\Sdk\Exceptions\JsonException;
use Jtl\Fulfillment\Api\Sdk\Models\General\CarrierCode;
use Jtl\Fulfillment\Api\Sdk\Models\Pagination;
use Jtl\Fulfillment\Api\Sdk\Models\Query;
use Jtl\Fulfillment\Api\Sdk\Resources\Resource;

/**
 * Class CarrierCodeResource
 * @package Jtl\Fulfillment\Api\Sdk\Resources\General
 */
class CarrierCodeResource extends Resource
{
    /**
     * @param Query $query
     * @return Pagination
     * @throws Throwable
     * @throws JsonException
     */
    public function all(Query $query): Pagination
    {
        return $this->findAll('carriercodes', CarrierCode::class, $query, $this->buildCacheKey((string) $query));
    }
}
