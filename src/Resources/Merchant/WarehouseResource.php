<?php
namespace Jtl\Fulfillment\Api\Sdk\Resources\Merchant;

use Throwable;
use Izzle\Model\Model;
use Jtl\Fulfillment\Api\Sdk\Exceptions\JsonException;
use Jtl\Fulfillment\Api\Sdk\Models\Merchant\Warehouse\Warehouse;
use Jtl\Fulfillment\Api\Sdk\Models\Pagination;
use Jtl\Fulfillment\Api\Sdk\Models\Query;
use Jtl\Fulfillment\Api\Sdk\Resources\Resource;

/**
 * Class WarehouseResource
 * @package Jtl\Fulfillment\Api\Sdk\Resources\Merchant
 */
class WarehouseResource extends Resource
{
    /**
     * @param Query $query
     * @return Pagination.
     * @throws Throwable
     */
    public function all(Query $query): Pagination
    {
        return $this->findAll('merchant/warehouses', Warehouse::class, $query, $this->buildCacheKey((string) $query));
    }
    
    /**
     * @param string $id
     * @param Query|null $query
     * @return Warehouse|Model|null
     * @throws Throwable
     * @throws JsonException
     */
    public function find(string $id, Query $query = null): ?Model
    {
        return $this->findBy(
            sprintf('merchant/warehouses/%s', $id),
            Warehouse::class,
            $this->buildCacheKey($id),
            $query
        );
    }
}
