<?php
namespace Jtl\Fulfillment\Api\Sdk\Resources\Merchant;

use Throwable;
use Izzle\Model\Model;
use Jtl\Fulfillment\Api\Sdk\Exceptions\JsonException;
use Jtl\Fulfillment\Api\Sdk\Models\Pagination;
use Jtl\Fulfillment\Api\Sdk\Models\Query;
use Jtl\Fulfillment\Api\Sdk\Models\General\ShippingMethod;
use Jtl\Fulfillment\Api\Sdk\Resources\Resource;

/**
 * Class ShippingMethodResource
 * @package Jtl\Fulfillment\Api\Sdk\Resources\Merchant
 */
class ShippingMethodResource extends Resource
{
    /**
     * @param Query $query
     * @return Pagination
     * @throws Throwable
     * @throws JsonException
     */
    public function all(Query $query): Pagination
    {
        return $this->findAll(
            'merchant/shippingmethods',
            ShippingMethod::class,
            $query,
            $this->buildCacheKey((string) $query)
        );
    }
    
    /**
     * @param string $id
     * @param Query|null $query
     * @return ShippingMethod|Model|null
     * @throws Throwable
     * @throws JsonException
     */
    public function find(string $id, Query $query = null): ?Model
    {
        return $this->findBy(
            sprintf('merchant/shippingmethods/%s', $id),
            ShippingMethod::class,
            $this->buildCacheKey($id),
            $query
        );
    }
}
