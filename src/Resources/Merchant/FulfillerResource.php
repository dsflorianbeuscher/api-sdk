<?php
namespace Jtl\Fulfillment\Api\Sdk\Resources\Merchant;

use Throwable;
use Izzle\Model\Model;
use Jtl\Fulfillment\Api\Sdk\Exceptions\JsonException;
use Jtl\Fulfillment\Api\Sdk\Models\Merchant\Fulfiller\Fulfiller;
use Jtl\Fulfillment\Api\Sdk\Models\Pagination;
use Jtl\Fulfillment\Api\Sdk\Models\Query;
use Jtl\Fulfillment\Api\Sdk\Resources\Resource;

/**
 * Class FulfillerResource
 * @package Jtl\Fulfillment\Api\Sdk\Resources\Merchant
 */
class FulfillerResource extends Resource
{
    /**
     * @param Query $query
     * @return Pagination
     * @throws Throwable
     * @throws JsonException
     */
    public function all(Query $query): Pagination
    {
        return $this->findAll('merchant/fulfillers', Fulfiller::class, $query, $this->buildCacheKey((string) $query));
    }
    
    /**
     * @param string $id
     * @param Query|null $query
     * @return Fulfiller|Model|null
     * @throws Throwable
     * @throws JsonException
     */
    public function find(string $id, Query $query = null): ?Model
    {
        return $this->findBy(sprintf('merchant/fulfillers/%s', $id), Fulfiller::class, $this->buildCacheKey($id), $query);
    }
}
