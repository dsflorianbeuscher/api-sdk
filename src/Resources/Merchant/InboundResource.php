<?php
namespace Jtl\Fulfillment\Api\Sdk\Resources\Merchant;

use Jtl\Fulfillment\Api\Sdk\Exceptions\HttpException;
use Throwable;
use InvalidArgumentException;
use Jtl\Fulfillment\Api\Sdk\Models\Merchant\Inbound\InboundShippingNotification;
use Jtl\Fulfillment\Api\Sdk\Models\TimeFrame;
use Jtl\Fulfillment\Api\Sdk\Resources\Resource;
use Izzle\Model\Model;
use Jtl\Fulfillment\Api\Sdk\Exceptions\JsonException;
use Jtl\Fulfillment\Api\Sdk\Models\Merchant\Inbound\Inbound;
use Jtl\Fulfillment\Api\Sdk\Models\Pagination;
use Jtl\Fulfillment\Api\Sdk\Models\Query;

class InboundResource extends Resource
{
    /**
     * @param Query $query
     * @return Pagination
     * @throws Throwable
     * @throws JsonException
     */
    public function all(Query $query): Pagination
    {
        return $this->findAll('merchant/inbounds', Inbound::class, $query, $this->buildCacheKey((string) $query));
    }
    
    /**
     * @param string $inboundId
     * @param Query|null $query
     * @return Inbound|Model|null
     * @throws Throwable
     * @throws JsonException
     */
    public function find(string $inboundId, Query $query = null): ?Model
    {
        return $this->findBy(sprintf('merchant/inbounds/%s', $inboundId), Inbound::class, $this->buildCacheKey($inboundId), $query);
    }
    
    /**
     * @param Inbound|Model $model
     * @param Query|null $query
     * @return bool
     * @throws Throwable
     * @throws JsonException
     */
    public function save(Model $model, Query $query = null): bool
    {
        $this->validModel($model, Inbound::class);
        
        /** @var Inbound $model */
        return empty($model->getInboundId()) ?
            $this->create($model, 'merchant/inbounds', Inbound::class, $model->property('inboundId')) :
            $this->update(
                $model,
                sprintf('merchant/inbounds/%s', $model->getInboundId()),
                $this->buildCacheKey($model->getInboundId())
            );
    }
    
    /**
     * @param string $inboundId
     * @return bool
     * @throws Throwable
     */
    public function close(string $inboundId): bool
    {
        try {
            $response = $this->getClient()->getHttp()->request(
                'PUT',
                sprintf('merchant/inbounds/%s/close', $inboundId)
            );
    
            $result = $response->getStatusCode() === 200;
            if ($result) {
                $this->getResourceCache()->delete($this->buildCacheKey($inboundId));
        
                // Delete Page Cache
                $this->deletePageCache();
            }
    
            return $result;
        } catch (Throwable $e) {
            HttpException::handleGuzzeException($e);
        }
    
        return false;
    }
    
    /**
     * @param string $inboundId
     * @return Pagination
     * @throws Throwable
     * @throws JsonException
     */
    public function findShippingNotifications(string $inboundId): Pagination
    {
        $query = new Query();
    
        return $this->findAll(
            sprintf('merchant/inbounds/%s/shipping-notifications', $inboundId),
            InboundShippingNotification::class,
            $query,
            $this->buildCacheKey((string) $query)
        );
    }
    
    /**
     * @param InboundShippingNotification $notification
     * @return bool
     * @throws JsonException
     * @throws Throwable
     */
    public function createShippingNotification(InboundShippingNotification $notification): bool
    {
        return $this->create(
            $notification,
            sprintf('merchant/inbounds/%s/shipping-notifications', $notification->getInboundId()),
            InboundShippingNotification::class,
            $notification->property('inboundShippingNotificationId')
        );
    }
    
    /**
     * @param string $inboundId
     * @param string $inboundShippingNotificationId
     * @return InboundShippingNotification|null
     * @throws Throwable
     * @throws JsonException
     */
    public function findShippingNotificationById(
        string $inboundId,
        string $inboundShippingNotificationId
    ): ?InboundShippingNotification {
        $cacheKey = $this->buildCacheKey($inboundShippingNotificationId);
    
        // Try Cache
        $cachedItem = $this->getResourceCache()->get($cacheKey);
        if ($cachedItem !== null) {
            return $cachedItem;
        }
    
        try {
            $response = $this->getClient()->getHttp()->request(
                'GET',
                sprintf('merchant/inbounds/%s/shipping-notifications/%s', $inboundId, $inboundShippingNotificationId)
            );
    
            $notification = new InboundShippingNotification($this->extractData($response));
    
            // Set Cache
            $this->getResourceCache()->set($notification, $cacheKey);
    
            return $notification;
        } catch (Throwable $e) {
            HttpException::handleGuzzeException($e);
        }
        
        return null;
    }
    
    /**
     * @param string $inboundId
     * @param string $inboundShippingNotificationId
     * @return bool
     * @throws Throwable
     */
    public function removeShippingNotification(
        string $inboundId,
        string $inboundShippingNotificationId
    ): bool {
        try {
            $response = $this->getClient()->getHttp()->request(
                'DELETE',
                sprintf('merchant/inbounds/%s/shipping-notifications/%s', $inboundId, $inboundShippingNotificationId)
            );
    
            $result = $response->getStatusCode() === 200;
            if ($result) {
                $this->getResourceCache()->delete($this->buildCacheKey($inboundId));
                
                // Delete Page Cache
                $this->deletePageCache();
            }
    
            return $result;
        } catch (Throwable $e) {
            HttpException::handleGuzzeException($e);
        }
        
        return false;
    }
    
    /**
     * @param InboundShippingNotification $notification
     * @return bool
     * @throws Throwable
     */
    public function updateShippingNotification(InboundShippingNotification $notification): bool
    {
        if (empty($notification->getInboundId()) || empty($notification->getInboundShippingNotificationId())) {
            throw new InvalidArgumentException('Missing primary keys');
        }
    
        try {
            $response = $this->getClient()->getHttp()->request(
                'PATCH',
                sprintf('merchant/inbounds/%s/shipping-notifications/%s', $notification->getInboundId(), $notification->getInboundShippingNotificationId())
            );
        
            $result = $response->getStatusCode() === 204;
            if ($result) {
                $this->getResourceCache()->delete($this->buildCacheKey($notification->getInboundId()));
            
                // Delete Page Cache
                $this->deletePageCache();
            }
        
            return $result;
        } catch (Throwable $e) {
            HttpException::handleGuzzeException($e);
        }
        
        return false;
    }
    
    /**
     * @param Query $query
     * @return TimeFrame
     * @throws Throwable
     * @throws JsonException
     */
    public function findShippingNotificationsByTimeFrame(Query $query): TimeFrame
    {
        return $this->findUpdates(
            'merchant/inbounds/shipping-notifications/updates',
            InboundShippingNotification::class,
            $query,
            $this->buildCacheKey($query, 'merchant-isn-tf-')
        );
    }
}
