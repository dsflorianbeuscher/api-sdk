<?php
namespace Jtl\Fulfillment\Api\Sdk\Resources\Fulfiller;

use Throwable;
use Izzle\Model\Model;
use Jtl\Fulfillment\Api\Sdk\Exceptions\JsonException;
use Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Warehouse\Warehouse;
use Jtl\Fulfillment\Api\Sdk\Models\Pagination;
use Jtl\Fulfillment\Api\Sdk\Models\Query;
use Jtl\Fulfillment\Api\Sdk\Resources\Resource;

/**
 * Class WarehouseResource
 * @package Jtl\Fulfillment\Api\Sdk\Resources
 */
class WarehouseResource extends Resource
{
    /**
     * @param Query $query
     * @return Pagination
     * @throws Throwable
     */
    public function all(Query $query): Pagination
    {
        return $this->findAll('fulfiller/warehouses', Warehouse::class, $query, $this->buildCacheKey((string) $query));
    }
    
    /**
     * @param string $id
     * @param Query|null $query
     * @return Warehouse|Model|null
     * @throws Throwable
     * @throws JsonException
     */
    public function find(string $id, Query $query = null): ?Model
    {
        return $this->findBy(sprintf('fulfiller/warehouses/%s', $id), Warehouse::class, $this->buildCacheKey($id), $query);
    }
    
    /**
     * @param Warehouse|Model $model
     * @param Query|null $query
     * @return bool
     * @throws Throwable
     * @throws JsonException
     */
    public function save(Model $model, Query $query = null): bool
    {
        $this->validModel($model, Warehouse::class);
        
        /** @var Warehouse $model */
        return empty($model->getWarehouseId()) ?
            $this->create($model, 'fulfiller/warehouses', Warehouse::class, $model->property('warehouseId')) :
            $this->update(
                $model,
                sprintf('fulfiller/warehouses/%s', $model->getWarehouseId()),
                $this->buildCacheKey($model->getWarehouseId())
            );
    }
    
    /**
     * @param Model $model
     * @param Query|null $query
     * @return bool
     * @throws Throwable
     */
    public function remove(Model $model, Query $query = null): bool
    {
        $this->validModel($model, Warehouse::class);
        
        /** @var Warehouse $model */
        return $this->delete(
            sprintf('fulfiller/warehouses/%s', $model->getWarehouseId()),
            $this->buildCacheKey($model->getWarehouseId())
        );
    }
}
