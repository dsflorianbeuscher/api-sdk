<?php
namespace Jtl\Fulfillment\Api\Sdk\Resources\Fulfiller;

use Izzle\Model\Model;
use Jtl\Fulfillment\Api\Sdk\Exceptions\JsonException;
use Jtl\Fulfillment\Api\Sdk\Models\General\ShippingMethod;
use Jtl\Fulfillment\Api\Sdk\Models\Pagination;
use Jtl\Fulfillment\Api\Sdk\Models\Query;
use Jtl\Fulfillment\Api\Sdk\Resources\Resource;
use Throwable;

/**
 * Class ShippingMethodResource
 * @package Jtl\Fulfillment\Api\Sdk\Resources\Fulfiller
 */
class ShippingMethodResource extends Resource
{
    /**
     * @param Query $query
     * @return Pagination
     * @throws Throwable
     */
    public function all(Query $query): Pagination
    {
        return $this->findAll(
            'fulfiller/shippingmethods',
            ShippingMethod::class,
            $query,
            $this->buildCacheKey((string) $query)
        );
    }
    
    /**
     * @param ShippingMethod|Model $model
     * @param Query|null $query
     * @return bool
     * @throws Throwable
     * @throws JsonException
     */
    public function save(Model $model, Query $query = null): bool
    {
        $this->validModel($model, ShippingMethod::class);
        
        return $this->create(
            $model,
            'fulfiller/shippingmethods',
            ShippingMethod::class,
            $model->property('shippingMethodId')
        );
    }
    
    /**
     * @param ShippingMethod|Model $model
     * @param Query|null $query
     * @return bool
     * @throws Throwable
     */
    public function remove(Model $model, Query $query = null): bool
    {
        $this->validModel($model, ShippingMethod::class);
        
        /** @var ShippingMethod $model */
        return $this->delete(
            sprintf('fulfiller/shippingmethods/%s', $model->getShippingMethodId()),
            $this->buildCacheKey($model->getShippingMethodId())
        );
    }
    
    /**
     * @param string $id
     * @param Query|null $query
     * @return ShippingMethod|Model|null
     * @throws Throwable
     * @throws JsonException
     */
    public function find(string $id, Query $query = null): ?Model
    {
        return $this->findBy(
            sprintf('fulfiller/shippingmethods/%s', $id),
            ShippingMethod::class,
            $this->buildCacheKey($id),
            $query
        );
    }
}
