<?php
namespace Jtl\Fulfillment\Api\Sdk\Resources\Fulfiller;

use Izzle\Model\Model;
use Jtl\Fulfillment\Api\Sdk\Exceptions\HttpException;
use Jtl\Fulfillment\Api\Sdk\Exceptions\JsonException;
use Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Authorization\Authorization;
use Jtl\Fulfillment\Api\Sdk\Models\Pagination;
use Jtl\Fulfillment\Api\Sdk\Models\Query;
use Jtl\Fulfillment\Api\Sdk\Resources\Resource;
use Throwable;

/**
 * Class AuthorizationResource
 * @package Jtl\Fulfillment\Api\Sdk\Resources\Fulfiller
 */
class AuthorizationResource extends Resource
{
    /**
     * @param Query $query
     * @return Pagination
     * @throws Throwable
     * @throws JsonException
     */
    public function all(Query $query): Pagination
    {
        return $this->findAll(
            'fulfiller/authorizations',
            Authorization::class,
            $query,
            $this->buildCacheKey((string) $query)
        );
    }
    
    /**
     * @param string $merchantId
     * @return Model|Authorization|null
     * @throws Throwable
     * @throws JsonException
     */
    public function findByMerchantId(string $merchantId): ?Authorization
    {
        return $this->findBy(
            sprintf('fulfiller/authorizations/%s', $merchantId),
            Authorization::class,
            $this->buildCacheKey($merchantId)
        );
    }
    
    /**
     * @param string $merchantId
     * @param string $warehouseId
     * @return Authorization
     * @throws Throwable
     * @throws JsonException
     */
    public function authorizeMerchant(string $merchantId, string $warehouseId): Authorization
    {
        $response = $this->getClient()->getHttp()->request(
            'POST',
            sprintf('fulfiller/authorizations/%s/warehouses', $merchantId),
            [
                'body' => json_encode(['warehouseId' => $warehouseId])
            ]
        );
    
        $this->getResourceCache()->delete($this->buildCacheKey($merchantId));
        
        return new Authorization($this->extractData($response));
    }
    
    /**
     * @param string $merchantId
     * @param string $warehouseId
     * @param string $shippingMethodId
     * @return Authorization
     * @throws Throwable
     * @throws JsonException
     */
    public function assignShippingMethod(string $merchantId, string $warehouseId, string $shippingMethodId): Authorization
    {
        $response = $this->getClient()->getHttp()->request(
            'POST',
            sprintf('fulfiller/authorizations/%s/warehouses/%s/shippingMethods', $merchantId, $warehouseId),
            [
                'body' => json_encode(['shippingMethodId' => $shippingMethodId])
            ]
        );
    
        $this->getResourceCache()->delete($this->buildCacheKey($merchantId));
        
        return new Authorization($this->extractData($response));
    }
    
    /**
     * @param string $merchantId
     * @param string $warehouseId
     * @return bool
     * @throws Throwable
     */
    public function removeAuthorization(string $merchantId, string $warehouseId): bool
    {
        try {
            $response = $this->getClient()->getHttp()->request(
                'DELETE',
                sprintf('fulfiller/authorizations/%s/warehouses/%s', $merchantId, $warehouseId)
            );
    
            $result = $response->getStatusCode() === 200;
    
            if ($result) {
                $this->getResourceCache()->delete($this->buildCacheKey($merchantId));
            }
    
            return $result;
        } catch (Throwable $e) {
            HttpException::handleGuzzeException($e);
        }
        
        return false;
    }
    
    /**
     * @param string $merchantId
     * @param string $warehouseId
     * @param string $shippingMethodId
     * @return bool
     * @throws Throwable
     */
    public function removeShippingMethod(string $merchantId, string $warehouseId, string $shippingMethodId): bool
    {
        try {
            $response = $this->getClient()->getHttp()->request(
                'DELETE',
                sprintf(
                    'fulfiller/authorizations/%s/warehouses/%s/shippingMethods/%s',
                    $merchantId,
                    $warehouseId,
                    $shippingMethodId
                )
            );
    
            $result = $response->getStatusCode() === 200;
    
            if ($result) {
                $this->getResourceCache()->delete($this->buildCacheKey($merchantId));
            }
    
            return $result;
        } catch (Throwable $e) {
            HttpException::handleGuzzeException($e);
        }
    
        return false;
    }
}
