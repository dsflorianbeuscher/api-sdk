<?php
namespace Jtl\Fulfillment\Api\Sdk\Resources;

use DateTime;
use Exception;
use InvalidArgumentException;
use Izzle\Model\Model;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Client;
use Jtl\Fulfillment\Api\Sdk\Exceptions\HttpException;
use Jtl\Fulfillment\Api\Sdk\Exceptions\JsonException;
use Jtl\Fulfillment\Api\Sdk\Models\DataModel;
use Jtl\Fulfillment\Api\Sdk\Models\Link;
use Jtl\Fulfillment\Api\Sdk\Models\Page;
use Jtl\Fulfillment\Api\Sdk\Models\Pagination;
use Jtl\Fulfillment\Api\Sdk\Models\Query;
use Jtl\Fulfillment\Api\Sdk\Models\TimeChunk;
use Jtl\Fulfillment\Api\Sdk\Models\TimeFrame;
use Psr\Http\Message\ResponseInterface;
use Jtl\Fulfillment\Api\Sdk\Exceptions\NotImplementedException;
use Throwable;

/**
 * Class Resource
 * @package Jtl\Fulfillment\Api\Sdk\Resources
 */
abstract class Resource implements ResourceInterface
{
    /**
     * @var Client
     */
    protected Client $client;

    /**
     * @var ResourceCache
     */
    protected ResourceCache $resourceCache;

    /**
     * Resource constructor.
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
        $this->resourceCache = new ResourceCache($client->getCache());
    }

    /**
     * @return Client
     */
    public function getClient(): Client
    {
        return $this->client;
    }

    /**
     * @return ResourceCache
     */
    public function getResourceCache(): ResourceCache
    {
        return $this->resourceCache;
    }

    /**
     * @param ResponseInterface|\GuzzleHttp\Message\Response $response
     * @return array
     * @throws JsonException
     */
    protected function extractData($response): array
    {
        $json = (string) $response->getBody();
        $data = json_decode($json, true);
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new JsonException(json_last_error_msg(), json_last_error());
        }

        return $data;
    }

    /**
     * @param array $data
     * @param string $class
     * @return Pagination
     * @throws InvalidArgumentException
     */
    public function getPagination(array $data, string $class): Pagination
    {
        if (!isset($data['items'])) {
            throw new InvalidArgumentException('Response does not have an items property');
        }

        if (!isset($data['_page'])) {
            throw new InvalidArgumentException('Response does not have an page property');
        }

        if (!isset($data['_links'])) {
            throw new InvalidArgumentException('Response does not have an links property');
        }

        if (!is_array($data['items'])) {
            throw new InvalidArgumentException('Property items must be an array');
        }

        $items = [];
        foreach ($data['items'] as $item) {
            $items[] = new $class($item);
        }

        return new Pagination([
            'items' => $items,
            'page' => new Page($data['_page']),
            'links' => new Link($data['_links']),
        ]);
    }

    /**
     * @param array $data
     * @param string $class
     * @return Pagination
     */
    protected function createPagination(array $data, string $class): Pagination
    {
        $items = [];
        foreach ($data as $item) {
            $items[] = new $class($item);
        }

        return new Pagination([
            'items' => $items,
            'page' => new Page(['total' => count($items)]),
            'links' => new Link(),
        ]);
    }

    /**
     * @param array $data
     * @param string $class
     * @return TimeFrame
     * @throws Exception
     */
    public function getTimeFrame(array $data, string $class): TimeFrame
    {
        if (!isset($data['data'])) {
            throw new InvalidArgumentException('Response does not have an data property');
        }

        if (!isset($data['from'])) {
            throw new InvalidArgumentException('Response does not have an from property');
        }

        if (!isset($data['to'])) {
            throw new InvalidArgumentException('Response does not have an to property');
        }

        if (!is_array($data['data'])) {
            throw new InvalidArgumentException('Property data must be an array');
        }

        $items = [];
        foreach ($data['data'] as $item) {
            $items[] = new $class($item);
        }

        return new TimeFrame([
            'data' => $items,
            'nextChunkUrl' => $data['nextChunkUrl'] ?? null,
            'from' => new DateTime($data['from']),
            'to' => new DateTime($data['to']),
        ]);
    }

    /**
     * @param array $data
     * @param string $class
     * @return TimeChunk
     * @throws Exception
     */
    public function getTimeChunk(array $data, string $class): TimeChunk
    {
        if (!isset($data['items'])) {
            throw new InvalidArgumentException('Response does not have an items property');
        }

        if (!isset($data['date'])) {
            throw new InvalidArgumentException('Response does not have an date property');
        }

        if (!is_array($data['items'])) {
            throw new InvalidArgumentException('Property items must be an array');
        }

        $items = [];
        foreach ($data['items'] as $item) {
            $items[] = new $class($item);
        }

        return new TimeChunk([
            'items' => $items,
            'nextChunkUrl' => $data['nextChunkUrl'] ?? null,
            'date' => new DateTime($data['date']),
            'moreDataAvailable' => $data['moreDataAvailable'] ?? false
        ]);
    }

    /**
     * @param Query $query
     * @return Pagination
     * @throws NotImplementedException
     */
    public function all(Query $query): Pagination
    {
        throw new NotImplementedException(sprintf('Method %s is not implemented', __METHOD__));
    }

    /**
     * @param string $id
     * @param Query|null $query
     * @return Model|null
     * @throws NotImplementedException
     */
    public function find(string $id, Query $query = null): ?Model
    {
        throw new NotImplementedException(sprintf('Method %s is not implemented', __METHOD__));
    }

    /**
     * @param Model $model
     * @param Query|null $query
     * @return bool
     * @throws NotImplementedException
     */
    public function save(Model $model, Query $query = null): bool
    {
        throw new NotImplementedException(sprintf('Method %s is not implemented', __METHOD__));
    }

    /**
     * @param Model $model
     * @param Query|null $query
     * @return bool
     * @throws NotImplementedException
     */
    public function remove(Model $model, Query $query = null): bool
    {
        throw new NotImplementedException(sprintf('Method %s is not implemented', __METHOD__));
    }

    /**
     * @param string $uri
     * @param string $class
     * @return TimeFrame
     * @throws Exception
     * @throws JsonException
     */
    public function nextChunk(string $uri, string $class): TimeFrame
    {
        $cacheKey = $this->buildCacheKey($uri, $class);

        // Try Cache
        $cachedItem = $this->resourceCache->get('chunk-' . $cacheKey);
        if ($cachedItem !== null) {
            return $cachedItem;
        }

        $response = $this->getClient()->getHttp()->request('GET', $uri);

        $timeFrame = $this->getTimeFrame($this->extractData($response), $class);

        // Set Cache
        $this->resourceCache->set($timeFrame, 'chunk-' . $cacheKey);

        return $timeFrame;
    }

    /**
     * @param DataModel $model
     * @param string $uri
     * @param string|null $class
     * @param PropertyInfo|null $pkProperty - Primary Key property info
     * @return bool
     * @throws Exception
     * @throws JsonException
     */
    protected function create(
        DataModel $model,
        string $uri,
        string $class = null,
        PropertyInfo $pkProperty = null
    ): bool {
        $response = $this->getClient()->getHttp()->request('POST', $uri, [
            'body' => json_encode(self::removeEmpty($model, $model->toArray())),
        ]);

        $id = null;
        if ($class !== null && $pkProperty !== null) {
            $setter = $pkProperty->setter();
            $getter = $pkProperty->getter();
            $entity = new $class($this->extractData($response));
            $id = $entity->{$getter}();
            $model->{$setter}($id);
        }

        $result = $response->getStatusCode() === 201;

        // Set Cache
        if ($result) {
            // Delete Page Cache
            $this->deletePageCache();

            if ($id !== null) {
                $this->resourceCache->set($model, $this->buildCacheKey($id));
            }
        }

        return $result;
    }

    /**
     * @param DataModel $model
     * @param string $uri
     * @param string|null $cacheKey
     * @return bool
     * @throws Exception
     */
    protected function update(DataModel $model, string $uri, string $cacheKey = null): bool
    {
        $response = $this->getClient()->getHttp()->request('PATCH', $uri, [
            'body' => json_encode(self::removeEmpty($model, $model->toArray())),
        ]);

        $result = $response->getStatusCode() === 204;

        // Set Cache
        if ($result) {
            // Delete Page Cache
            $this->deletePageCache();

            $this->resourceCache->set($model, $cacheKey);
        }

        return $result;
    }

    /**
     * @param string $uri
     * @param string|null $cacheKey
     * @return bool
     * @throws Exception
     */
    protected function delete(string $uri, string $cacheKey = null): bool
    {
        $response = $this->getClient()->getHttp()->request('DELETE', $uri);

        $result = $response->getStatusCode() === 204;

        if ($result) {
            // Delete Page Cache
            $this->deletePageCache();

            $this->resourceCache->delete($cacheKey);
        }

        return $result;
    }

    /**
     * @param string $uri
     * @param string $class
     * @param Query $query
     * @param string|null $cacheKey
     * @return Pagination
     * @throws JsonException
     * @throws Exception
     */
    protected function findAll(
        string $uri,
        string $class,
        Query $query,
        string $cacheKey = null
    ): Pagination {
        // Try Cache
        $cachedItem = $this->resourceCache->get('page-' . $cacheKey);
        if ($cachedItem !== null) {
            return $cachedItem;
        }

        // Try Http
        $response = $this->getClient()->getHttp()->request('GET', $uri, [
            'query' => $query->toArray(),
        ]);

        $data = $this->extractData($response);

        $pagination = isset($data['items']) ?
            $this->getPagination($data, $class) :
            $this->createPagination($data, $class);

        // Set Cache
        $this->resourceCache->set($pagination, 'page-' . $cacheKey);

        return $pagination;
    }

    /**
     * @param string $uri
     * @param string $class
     * @param Query $query
     * @param string|null $cacheKey
     * @return TimeFrame
     * @throws JsonException
     * @throws Exception
     * @throws Exception
     */
    protected function findUpdates(
        string $uri,
        string $class,
        Query $query,
        string $cacheKey = null
    ): TimeFrame {
        // Try Cache
        $cachedItem = $this->resourceCache->get('update-' . $cacheKey);
        if ($cachedItem !== null) {
            return $cachedItem;
        }

        // Try Http
        $tmp = Query::$oDataSerialization;
        Query::$oDataSerialization = false;

        $response = $this->getClient()->getHttp()->request('GET', $uri, [
            'query' => $query->toArray(),
        ]);

        Query::$oDataSerialization = $tmp;

        $timeFrame = $this->getTimeFrame($this->extractData($response), $class);

        // Set Cache
        $this->resourceCache->set($timeFrame, 'update-' . $cacheKey);

        return $timeFrame;
    }

    /**
     * @param string $uri
     * @param string $class
     * @param Query $query
     * @param string|null $cacheKey
     * @return TimeChunk
     * @throws JsonException
     */
    protected function findChunkUpdates(
        string $uri,
        string $class,
        Query $query,
        string $cacheKey = null
    ): TimeChunk {
        // Try Cache
        $cachedItem = $this->resourceCache->get('update-chunk-' . $cacheKey);
        if ($cachedItem !== null) {
            return $cachedItem;
        }

        // Try Http
        $tmp = Query::$oDataSerialization;
        Query::$oDataSerialization = false;

        $response = $this->getClient()->getHttp()->request('GET', $uri, [
            'query' => $query->toArray(),
        ]);

        Query::$oDataSerialization = $tmp;

        $timeChunk = $this->getTimeChunk($this->extractData($response), $class);

        // Set Cache
        $this->resourceCache->set($timeChunk, 'update-chunk-' . $cacheKey);

        return $timeChunk;
    }

    /**
     * @param string $uri
     * @param string $class
     * @param string|null $cacheKey
     * @param Query|null $query
     * @return Model|null
     * @throws Throwable
     */
    protected function findBy(string $uri, string $class, string $cacheKey = null, Query $query = null): ?Model
    {
        // Try Cache
        $cachedItem = $this->resourceCache->get($cacheKey);

        if ($cachedItem !== null) {
            return $cachedItem;
        }

        // Try Http
        try {
            $response = $this->getClient()->getHttp()->request('GET', $uri, [
                'query' => $query ? $query->toArray() : []
            ]);

            $model = new $class($this->extractData($response));

            // Set Cache
            $this->resourceCache->set($model, $cacheKey);

            return $model;
        } catch (Throwable $e) {
            HttpException::handleGuzzeException($e);

            return null;
        }
    }

    /**
     * @return bool
     */
    protected function deletePageCache(): bool
    {
        // Delete Page Cache
        //return $this->resourceCache->deleteMultiple($this->resourceCache->filterKeys('/^page-.+/'));
        return $this->resourceCache->clear();
    }

    /**
     * @param string|null $id
     * @param string $prefix
     * @return string|null
     */
    protected function buildCacheKey(?string $id, string $prefix = ''): ?string
    {
        if ($id === null) {
            return null;
        }

        return base64_encode(sprintf(
            '%s-%s-%s-%s',
            $prefix,
            $this->getClient()->getUserId(),
            get_class($this),
            $id
        ));
    }

    /**
     * @param DataModel $model
     * @param string $fqn
     */
    protected function validModel(DataModel $model, string $fqn): void
    {
        if (!is_a($model, $fqn)) {
            throw new InvalidArgumentException(
                sprintf(
                    'Expected parameter $model to be an instance of \'%s\', class \'%s\' given',
                    $fqn,
                    get_class($model)
                )
            );
        }
    }

    /**
     * Remove empty properties on data array
     *
     * @param DataModel $model
     * @param array $haystack
     * @return array
     */
    public static function removeEmpty(DataModel $model, array $haystack): array
    {
        foreach ($haystack as $key => $value) {
            if (is_array($value)) {
                $haystack[$key] = self::removeEmpty($model, $value);
            }

            if ($haystack[$key] === null ||
                (!$model->hasPreservable($key) && is_array($haystack[$key]) && count($haystack[$key]) === 0)
            ) {
                unset($haystack[$key]);
            }
        }

        return $haystack;
    }
}
