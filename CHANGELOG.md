CHANGELOG
=========

This changelog references the relevant changes (bug and security fixes)

* 0.24.0 (2022-07-05)
  
  * change [All] Push to izzle/model lib 0.11.0
  * change [All] Added PHP 8.1 compatibility

  * 0.23.2 (2022-02-23)
  
    * bug [Model] Added missing propertyInfos to BaseReturn Model

  * 0.23.1 (2022-02-04)
  
    * feature [Model] Added property "shippingLabelProviderData" to merchant Outbound Model

  * 0.23.0 (2022-01-11)
  
    * feature [Model] Added support for the new "Returns" Endpoint
    * feature [Resource] Added support for the new "Returns" Endpoint
    * feature [Model] Added relatedRetuns to Outbound Model
    * change [All] Push to >= PHP 7.4

  * 0.22.0 (2021-12-1)

    * feature [Model] Added preservable array to DataModel - Properties that are not touched during serialization
    * feature [Resource] Support for preservable array during serialization 
    * change [Resource] Removed serializationMode
    * change [Model] Removed serializationMode from DataModel

  * 0.21.1 (2021-12-1)
  
    * change [Resource] Removing debug logging

  * 0.21.0 (2021-11-30)
  
    * change [Model] Changed parent class from Izzle\Model\Model to own Jtl\Fulfillment\Api\Sdk\Models\DataModel in all models
    * feature [Model] Added serializationMode to DataModel for serialization handling in resources

  * 0.20.2 (2021-09-28)

    * bug [Tests] Fixed Seller SignUp Test

  * 0.20.1 (2021-09-28)

    * bug [Resource] Fixed minor Seller SignUp Response bugs

  * 0.20.0 (2021-09-27)
  
    * feature [Resource] Added new Seller SignUp API Resource
    * feature [Resource] Added new Stock Complete Call
    * change [Resource] Moved Stock Models to General Namespace

  * 0.19.0 (2021-05-18)

    * change [Resource] Fixed createToken AccessReource return Type to Token

  * 0.18.3 (2021-03-31)

    * bug [Example] Fixed missing changes in examples files

  * 0.18.2 (2021-03-31)

    * bug [Model] Fixed return value from shippingFee and orderValue on Outbound Model

  * 0.18.1 (2021-03-31)

    * feature [Model] Added cancelReasonCode, statusTimestamp, shippingFee and orderValue to General Outbound

  * 0.18.0 (2021-01-11)

    * change [Cache] Removed Symfony Cache

  * 0.17.5 (2021-01-11)

    * feature [Project] Added PHP 8.0 compatibility

  * 0.17.4 (2020-11-03)

      * feature [Model] Added createdAt to Token Model

  * 0.17.3 (2020-11-02)

      * bug [PSR-4] Fixed minor psr-4 autoload namespace bug

  * 0.17.2 (2020-11-02)

      * change [Resource] Added new Access Token Calls.

  * 0.17.1 (2020-11-02)

      * change [Resource] Added Guzzle Exception Handler.

  * 0.17.0 (2020-10-30)

      * change [All] Removed Guzzle extension dependencies.

  * 0.16.0 (2020-10-07)

      * change [Client] Removed Guzzle extension. Must be injected from extern.

  * 0.15.3 (2020-09-02)

      * change [Resource] Changed closeInbound to close on Fulfiller Inbound Resource
      * feature [Model] Added nullable boolean values to Product Specification
      * change [Builder] Changed value from string to mixed (string, int, null)

  * 0.15.2 (2020-09-02)

      * bug [Builder] Fixed methods orWhereEndsWith
      * bug [Grammar] Fixed contains, startswith and endswith in OData Grammar Builder

  * 0.15.1 (2020-09-02)

      * bug [Model] Added nullable netWeight to General Product Model

  * 0.15.0 (2020-09-02)

      * feature [Model] Added netWeight to General Product Model
      * change [Grammar] Changed OData value handling. Numeric values will not be escaped.

  * 0.14.0 (2020-08-27)

      * change [Resource] Changed Outbound and Inbound Resource PK from status to id

  * 0.13.3 (2020-07-15)

      * feature [Model] Added billOfMaterialsId to OutboundItem model

  * 0.13.2 (2020-07-13)

      * feature [Model] Added merchantSku and name to ProductBillOfMaterialsComponent model

  * 0.13.1 (2020-07-13)

     * bug [Query] Fixed method toODataArray where complex $expands and $filters where broken

  * 0.13.0 (2020-07-01)

      * feature [Resource] Added updateShippingNotification method to Inbound Resource
      * feature [Resource] Added updateShippingNotification method to Outbound Resource
      * feature [Model] Added inboundShippingNotificationId to InboundShippingNotification Model
      * feature [Model] Added outboundShippingNotificationId to OutboundShippingNotification Model
      * change [Resource] Changed merchantInboundNumber PK to inboundId on Inbound Resource
      * change [Resource] Changed merchantOutboundNumber PK to outboundId on Outbound Resource
      * change [Resource] Changed merchantShippingNotificationNumber PK to inboundShippingNotificationId on Inbound Resource
      * change [Resource] Changed merchantShippingNotificationNumber PK to outboundShippingNotificationId on Outbound Resource

  * 0.12.0 (2020-06-30)

      * change [Resource] Changed all merchantInboundNumber params to inboundId in Merchant Inbound Resource
      * change [Model] Added inboundId to General Inbound Model
      * change [Model] Dropped inboundId from Fulfiller Inbound Model
      * change [Resource] Changed all merchantOutboundNumber params to outboundId in Merchant Outbound Resource
      * change [Model] Added outboundId to General Outbound Model
      * change [Model] Dropped outboundId from Fulfiller Outbound Model

  * 0.11.5 (2020-06-25)

      * feature [Resource] Added grantOverallAccessTo method to product resource

  * 0.11.4 (2020-06-22)

      * feature [Model] Added setCurrentPage to Page Model

  * 0.11.3 (2020-04-27)

      * change [Model] Changed params to Query Model toArray

  * 0.11.2 (2020-04-27)

      * change [Model] Added merchantSku to ProductStockLevel Model
      * change [Model] Added params to Query Model toArray

  * 0.11.1 (2020-04-14)

      * feature [Model] Added merchantSku to StockChange

  * 0.11.0 (2020-01-24)
    
      * feature [Library] Push to Model Lib 0.8.0

  * 0.10.0 (2020-01-17)

      * feature [Model] Added merchantSku to InboundItem
      * changed [Model] Changed QueryFilter to QueryParam
      * feature [Model] Added Params (QueryFilter[]) to Query
      * changed [Resource] Changed save and remove Interface Params

  * 0.9.1 (2020-01-09)

      * feature [Resource] Added outbound shipped declaration

  * 0.9.0 (2020-01-02)

      * change [Resource] The Find method has been extended by a query parameter

  * 0.8.0 (2019-12-19)

      * change [Resource] Changed logic in Inbound Resource itemArrived

  * 0.7.5 (2019-12-16)

      * change [Composer] Force Guzzle Lib to 6.4.* cause 6.5 is broken

  * 0.7.4 (2019-11-25)

      * feature [Model] Added new constants

  * 0.7.3 (2019-11-21)

      * feature [Model] Added new methods to Page Object

  * 0.7.2 (2019-11-06)

      * change [Resource] Fixed update response code

  * 0.7.1 (2019-11-05)

      * feature [Library] Push to Model Lib 0.7.0

  * 0.7.0 (2019-11-04)

      * feature [Builder] Added new whereSub method
      * Fixed [Builder] whereSub method on array with objects
      * feature [Resource] Added outbound changes method
      * feature [Resource] Added allChanges method to merchant Stock resource
      * change [Model] General StockChanges splitted into merchant and fulfiller
      * change [Client] Added userId for unique caching context
      * change [Resource] Changed In- and Outbound findShippingNotifications from array to Pagination
      * feature [Builder] Added new $expand feature
      * feature [Model] Added openInbounds and openOutbounds to Product Model
      * change [Resource] Changed General Product Model to Merchant on Merchant Product Resource

  * 0.6.0 (2019-08-28)
    
      * feature [Resource] Added findAttachment method to Merchant Outbound Resource
      * feature [Resource] Added findShippingNotificationsByTimeFrame method to Merchant Outbound Resource
      * change [Model] Splitted global OutboundShippingNotification to merchant and fulfiller

  * 0.5.0 (2019-08-27)
    
      * bug [Resource] Fixed typo in Outbound Resource
      * change [Resource] Changed Merchant Credential Service
      * feature [Resource] Added Stock all and allByWarehouseId Methods

  * 0.4.1 (2019-08-15)
    
      * feature [Resource] Added all method to fulfiller merchant resource
      * feature [Builder] Added whereIn to Query Builder
      * bug [Test] Fixed Merchant Credential Test
      * feature [Model] Added new whereIn and orWhereIn Method to Query Model
      * feature [Model] Added carrierName and carrierCode to General Package Model
      * feature [Model] Added General Access Resource
    
  * 0.4.0 (2019-07-24)
      * change [Model] Changed General Package identifier from object to array of identifier
      * bug [Model] Added shippingMethodId to Outbound property info list
      * feature [Model] Changed Amazon SFP Credential Entity
      * change [Resource] Create method will remove empty properties from model data
 
  * 0.3.4 (2019-07-16)

      * feature [Model] Added nullables to all basic data types for unsetting api values
      * feature [Resource] Added Credential Resource with Amazon Calls
      * feature [Model] Added product bundles

  * 0.3.3 (2019-07-01)
    
      * feature [Model] Added carriercode Model
      * feature [Resource] Added carriercode Resource
      * feature [Resource] Added non pagination list support

  * 0.3.2 (2019-06-11)

      * feature [Model] Added role constants to user model
      * feature [Model] Added type constants to attribute model
      * feature [Model] Added status constants to outbound model
      * feature [Model] Added premium type constants to outbound model
      * feature [Model] Added shipping type constants to outbound model
      * feature [Model] Added item type constants to outbound item model

  * 0.3.1 (2019-05-22)

      * feature [Model] Added fillData method to ProductPicture class
      * feature [Model] Added loadDataFromUrl method to ProductResource class
      * feature [LICENSE] Changed license to MIT

  * 0.3.0 (2019-05-15)
    
      * feature [Model] Renamed isSetItemMaster to isBillOfMaterials on ProductSpecification
      * feature [Model] Renamed setItems to billOfMaterialsComponents on ProductSpecification
      * feature [Model] Renamed class ProductSetItem to ProductBillOfMaterialsComponent

  * 0.2.1 (2019-05-15)

      * feature [Project] Added php-cs-fixer support
      * feature [Model] Added more Builder support for Query model
      * feature [Builder] Added new api filter support
      * feature [Grammar] Added new api OData filter support
      * feature [Model] Added default string enum values

  * 0.2.0 (2019-05-14)

      * feature [Model] Added filter query builder and compiler
      * feature [Model] Moved StockChange and StockChangeId model to general namespace
      * feature [Model] Changed StockChange to new structure
      * feature [Model] Added fromDate, toDate and Page property to Query model
      * feature [Resource] Added new findUpdates method to fulfiller and merchant resource
      * feature [Builder] Added query builder array support for where / orWhere method (#FFN-299)
      * feature [Grammar] Added new OData Grammar
      * feature [Code] Removed some warnings
      * feature [Model] Added OData Support to Query Model
      * feature [Model] Changed firstname and lastname to camel case

  * 0.1.0 (2019-04-15)

      * feature [Model] Removed epid property from product identifier
      * feature [Model] Added publicUrl property to product picture
      * feature [Model] Changed purchasePrice property to netRetailPrice on product picture
      * feature [Resource] Added new findPictureByProductId method to global product resource
