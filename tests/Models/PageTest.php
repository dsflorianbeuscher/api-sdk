<?php
namespace Jtl\Fulfillment\Api\Sdk\Test\Models;

use Jtl\Fulfillment\Api\Sdk\Models\Page;
use PHPUnit\Framework\TestCase;

/**
 * Class ResourceTest
 * @package Jtl\Fulfillment\Api\Sdk\Test\Resources
 */
class PageTest extends TestCase
{
    public function testCanBeInitialized(): void
    {
        $page = new Page([
            'total' => 14,
            'limit' => 10,
            'offset' => 0
        ]);
        
        $this->assertInstanceOf(Page::class, $page);
        $this->assertEquals([
            'total' => 14,
            'limit' => 10,
            'offset' => 0,
            'lastPage' => 2,
            'currentPage' => 1,
            'from' => 1,
            'to' => 10
        ], $page->toArray());
    
        $page = new Page([
            'total' => 14,
            'limit' => 10,
            'offset' => 10
        ]);
    
        $this->assertEquals([
            'total' => 14,
            'limit' => 10,
            'offset' => 10,
            'lastPage' => 2,
            'currentPage' => 2,
            'from' => 11,
            'to' => 14
        ], $page->toArray());
    
        $page = new Page([
            'total' => 3782,
            'limit' => 100,
            'offset' => 200
        ]);
    
        $this->assertEquals([
            'total' => 3782,
            'limit' => 100,
            'offset' => 200,
            'lastPage' => 38,
            'currentPage' => 3,
            'from' => 201,
            'to' => 300
        ], $page->toArray());
    
        $page = new Page([
            'total' => 3782,
            'limit' => 100,
            'currentPage' => 3
        ]);
    
        $this->assertEquals([
            'total' => 3782,
            'limit' => 100,
            'offset' => 200,
            'lastPage' => 38,
            'currentPage' => 3,
            'from' => 201,
            'to' => 300
        ], $page->toArray());
    }
}
