<?php
namespace Jtl\Fulfillment\Api\Sdk\Test\Resources\Merchant;

use GuzzleHttp\Exception\ClientException;
use Jtl\Fulfillment\Api\Sdk\Models\Merchant\Amazon\Seller;
use Jtl\Fulfillment\Api\Sdk\Resources\Merchant\SellerSignUpResource;
use Jtl\Fulfillment\Api\Sdk\Test\Resources\AbstractResourceTest;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Jtl\Fulfillment\Api\Sdk\Client;
use GuzzleHttp\Client as HttpClient;

/**
 * Class SellerSignUpTest
 * @package Jtl\Fulfillment\Api\Sdk\Test\Resources\Merchant
 */
class SellerSignUpTest extends AbstractResourceTest
{
    public function testCanInitSignUp(): void
    {
        $mock = new MockHandler([
            new Response(200, [], file_get_contents(__DIR__ . '/MockData/seller_signup_init.json')),
            new Response(403)
        ]);

        $handler = HandlerStack::create($mock);
        $client = new Client(new HttpClient(['handler' => $handler]));

        $resource = new SellerSignUpResource($client);

        $signUp = $resource->initSignUp(12, 'Foobar', 'A1PA6795UKMFR9', 'https://www.google.de');
        self::assertEquals('https://ea-amazon.api.jtl-software.com/ffn/amazon/auth?session=****', $signUp->getRedirectUrl());
        self::assertEquals(3600, $signUp->getExpiresIn());

        $this->expectException(ClientException::class);
        $resource->initSignUp(12, 'Foobar', 'A1PA6795UKMFR9', 'https://www.google.de');
    }

    public function testCanQuerySellers(): void
    {
        $mock = new MockHandler([
            new Response(200, [], file_get_contents(__DIR__ . '/MockData/seller_signup_seller_all.json')),
            new Response(403)
        ]);

        $handler = HandlerStack::create($mock);
        $client = new Client(new HttpClient(['handler' => $handler]));

        $resource = new SellerSignUpResource($client);

        /** @var Seller[] $sellers */
        $sellers = $resource->getSellers(12);
        self::assertCount(1, $sellers);
        self::assertInstanceOf(Seller::class, $sellers[0]);

        $this->expectException(ClientException::class);
        $resource->getSellers(12);
    }
}
