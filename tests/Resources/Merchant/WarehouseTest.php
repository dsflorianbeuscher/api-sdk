<?php
namespace Jtl\Fulfillment\Api\Sdk\Test\Resources\Merchant;

use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\Merchant\Warehouse\Warehouse;
use Jtl\Fulfillment\Api\Sdk\Resources\Merchant\WarehouseResource;
use Jtl\Fulfillment\Api\Sdk\Test\Resources\AbstractResourceTest;

/**
 * Class WarehouseTest
 * @package Jtl\Fulfillment\Api\Sdk\Test\Resources\Merchant
 */
class WarehouseTest extends AbstractResourceTest
{
    public function testCanQueryAll(): void
    {
        $this->canQueryAll(__DIR__ . '/MockData/warehouse_all.json', WarehouseResource::class, Warehouse::class);
    }
    
    public function testCanFindOne(): void
    {
        $this->canFindOne(
            __DIR__ . '/MockData/warehouse_one.json',
            WarehouseResource::class,
            Warehouse::class,
            'FULF04XX-12345-0001',
            new PropertyInfo('warehouseId')
        );
    }
}
