<?php
namespace Jtl\Fulfillment\Api\Sdk\Test\Resources\Merchant;

use GuzzleHttp\Exception\ClientException;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\Merchant\Product\Product;
use Jtl\Fulfillment\Api\Sdk\Models\General\Product\ProductPicture;
use Jtl\Fulfillment\Api\Sdk\Models\Merchant\Product\ProductAuthorization;
use Jtl\Fulfillment\Api\Sdk\Resources\Merchant\ProductResource;
use Jtl\Fulfillment\Api\Sdk\Test\Resources\AbstractResourceTest;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Jtl\Fulfillment\Api\Sdk\Client;
use GuzzleHttp\Client as HttpClient;

/**
 * Class ProductTest
 * @package Jtl\Fulfillment\Api\Sdk\Test\Resources\Merchant
 */
class ProductTest extends AbstractResourceTest
{
    public function testCanQueryAll(): void
    {
        $this->canQueryAll(__DIR__ . '/MockData/product_all.json', ProductResource::class, Product::class);
    }
    
    public function testCanFindOne(): void
    {
        $this->canFindOne(
            __DIR__ . '/MockData/product_one.json',
            ProductResource::class,
            Product::class,
            'MERC01PRDCT',
            new PropertyInfo('jfsku')
        );
    }
    
    public function testCanUpdate(): void
    {
        $this->canUpdate(
            __DIR__ . '/MockData/product_one.json',
            ProductResource::class,
            Product::class,
            'MERC01PRDCT',
            new PropertyInfo('jfsku')
        );
    }
    
    public function testCanCreate(): void
    {
        $this->canCreate(
            __DIR__ . '/MockData/product_one.json',
            ProductResource::class,
            Product::class,
            'MERC01PRDCT',
            new PropertyInfo('jfsku')
        );
    }
    
    public function testCanRemove(): void
    {
        $this->canRemove(
            __DIR__ . '/MockData/product_one.json',
            ProductResource::class,
            Product::class
        );
    }
    
    public function testCanReceiveAuthorizations(): void
    {
        $mock = new MockHandler([
            new Response(200, [], file_get_contents(__DIR__ . '/MockData/product_authorization_all.json')),
            new Response(403),
            new Response(404)
        ]);
    
        $handler = HandlerStack::create($mock);
        $client = new Client(new HttpClient(['handler' => $handler]));
        
        $resource = new ProductResource($client);
    
        $authorizations = $resource->findAuthorizations('MERC01PRDCT');
        
        $this->assertCount(1, $authorizations);
        $this->assertInstanceOf(ProductAuthorization::class, $authorizations[0]);
    
        $this->expectException(ClientException::class);
        $resource->findAuthorizations('MERC01PRDCT');
        
        $this->assertEmpty($resource->findAuthorizations('MERC01PRDCT'));
    }
    
    public function testCanGrantAccessToFulfiller(): void
    {
        $mock = new MockHandler([
            new Response(200, [], file_get_contents(__DIR__ . '/MockData/product_authorization_one.json')),
            new Response(403),
            new Response(404)
        ]);
    
        $handler = HandlerStack::create($mock);
        $client = new Client(new HttpClient(['handler' => $handler]));
    
        $resource = new ProductResource($client);
    
        $authorization = $resource->grantAccess('MERC01PRDCT', 'FULF');
    
        $this->assertInstanceOf(ProductAuthorization::class, $authorization);
        $this->assertEquals('FULF', $authorization->getFulfillerId());
    
        $this->expectException(ClientException::class);
        $resource->grantAccess('MERC01PRDCT', 'FULF');
    
        $this->assertEmpty($resource->grantAccess('MERC01PRDCT', 'FULF'));
    }
    
    public function testAuthorizationCanBeRemoved(): void
    {
        $mock = new MockHandler([
            new Response(200),
            new Response(403),
            new Response(404)
        ]);
    
        $handler = HandlerStack::create($mock);
        $client = new Client(new HttpClient(['handler' => $handler]));
    
        $resource = new ProductResource($client);
    
        $this->assertTrue($resource->removeAccess('MERC01PRDCT', 'FULF'));
    
        $this->expectException(ClientException::class);
        $resource->removeAccess('MERC01PRDCT', 'FULF');
    
        $this->assertFalse($resource->removeAccess('MERC01PRDCT', 'FULF'));
    }
    
    public function testCangrantOverallAccessToFulFiller(): void
    {
        $mock = new MockHandler([
            new Response(201),
            new Response(401),
            new Response(503)
        ]);
    
        $handler = HandlerStack::create($mock);
        $client = new Client(new HttpClient(['handler' => $handler]));
    
        $resource = new ProductResource($client);
    
        $this->assertTrue($resource->grantOverallAccessTo('FULF'));
    
        $this->expectException(ClientException::class);
        $resource->grantOverallAccessTo('FULF');
    
        $this->expectException(ClientException::class);
        $this->assertFalse($resource->grantOverallAccessTo('FULF'));
    }
    
    public function testCanAddAPicture(): void
    {
        $mock = new MockHandler([
            new Response(200, [], file_get_contents(__DIR__ . '/MockData/product_picture_one.json')),
            new Response(403),
            new Response(404)
        ]);
    
        $handler = HandlerStack::create($mock);
        $client = new Client(new HttpClient(['handler' => $handler]));
    
        $resource = new ProductResource($client);
    
        /** @var ProductPicture $model */
        $model = $resource->addPicture('ABC', 2);
    
        $this->assertInstanceOf(ProductPicture::class, $model);
        
        $this->expectException(ClientException::class);
        $resource->addPicture('ABC', 2);
    
        $this->assertFalse($resource->addPicture('ABC', 2));
    }
    
    public function testCanFindPicture(): void
    {
        $mock = new MockHandler([
            new Response(200, [], file_get_contents(__DIR__ . '/MockData/product_picture_one.json')),
            new Response(403),
            new Response(404)
        ]);
    
        $handler = HandlerStack::create($mock);
        $client = new Client(new HttpClient(['handler' => $handler]));
    
        $resource = new ProductResource($client);
    
        $model = $resource->findPicture('ABC', 2);
    
        $this->assertInstanceOf(ProductPicture::class, $model);
    
        $this->expectException(ClientException::class);
        $resource->findPicture('ABC', 2);
    
        $this->assertEmpty($resource->findPicture('ABC', 2));
    }
    
    public function testPictureCanBeRemoved(): void
    {
        $mock = new MockHandler([
            new Response(200),
            new Response(403),
            new Response(404)
        ]);
        
        $handler = HandlerStack::create($mock);
        $client = new Client(new HttpClient(['handler' => $handler]));
        
        $resource = new ProductResource($client);
        
        $this->assertTrue($resource->removePicture('ABC', 2));
        
        $this->expectException(ClientException::class);
        $resource->removePicture('ABC', 2);
        
        $this->assertFalse($resource->removePicture('ABC', 2));
    }
}
