<?php
namespace Jtl\Fulfillment\Api\Sdk\Test\Resources\Fulfiller;

use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Warehouse\Warehouse;
use Jtl\Fulfillment\Api\Sdk\Resources\Fulfiller\WarehouseResource;
use Jtl\Fulfillment\Api\Sdk\Test\Resources\AbstractResourceTest;

/**
 * Class WarehouseTest
 * @package Jtl\Fulfillment\Api\Sdk\Test\Resources\Fulfiller
 */
class WarehouseTest extends AbstractResourceTest
{
    public function testCanQueryAll(): void
    {
        $this->canQueryAll(__DIR__ . '/MockData/warehouse_all.json', WarehouseResource::class, Warehouse::class);
    }
    
    public function testCanFindOne(): void
    {
        $this->canFindOne(
            __DIR__ . '/MockData/warehouse_one.json',
            WarehouseResource::class,
            Warehouse::class,
            'FULF04XX-12345-0001',
            new PropertyInfo('warehouseId')
        );
    }
    
    public function testCanUpdate(): void
    {
        $this->canUpdate(
            __DIR__ . '/MockData/warehouse_one.json',
            WarehouseResource::class,
            Warehouse::class,
            'FULF04XX-12345-0001',
            new PropertyInfo('warehouseId')
        );
    }
    
    public function testCanCreate(): void
    {
        $this->canCreate(
            __DIR__ . '/MockData/warehouse_one.json',
            WarehouseResource::class,
            Warehouse::class,
            'FULF04XX-12345-0001',
            new PropertyInfo('warehouseId')
        );
    }
    
    public function testCanRemove(): void
    {
        $this->canRemove(
            __DIR__ . '/MockData/warehouse_one.json',
            WarehouseResource::class,
            Warehouse::class
        );
    }
}
