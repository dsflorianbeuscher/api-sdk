<?php
namespace Jtl\Fulfillment\Api\Sdk\Test\Resources\Fulfiller;

use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Client;
use Jtl\Fulfillment\Api\Sdk\Models\Fulfiller\Product\Product;
use Jtl\Fulfillment\Api\Sdk\Models\General\Product\ProductPicture;
use Jtl\Fulfillment\Api\Sdk\Resources\Fulfiller\ProductResource;
use Jtl\Fulfillment\Api\Sdk\Test\Resources\AbstractResourceTest;
use GuzzleHttp\Client as HttpClient;

/**
 * Class ProductTest
 * @package Jtl\Fulfillment\Api\Sdk\Test\Resources\Fulfiller
 */
class ProductTest extends AbstractResourceTest
{
    public function testCanQueryAll(): void
    {
        $this->canQueryAll(__DIR__ . '/MockData/product_all.json', ProductResource::class, Product::class);
    }
    
    public function testCanFindOneBySku(): void
    {
        $this->canFindOneBy(
            __DIR__ . '/MockData/product_one.json',
            ProductResource::class,
            Product::class,
            'findBySku',
            'FULF04XX',
            new PropertyInfo('jfsku')
        );
    }
    
    public function testCanFindPicture(): void
    {
        $mock = new MockHandler([
            new Response(200, [], file_get_contents(__DIR__ . '/MockData/product_picture_one.json')),
            new Response(403),
            new Response(404)
        ]);
    
        $handler = HandlerStack::create($mock);
        $client = new Client(new HttpClient(['handler' => $handler]));
    
        $resource = new ProductResource($client);
    
        $model = $resource->findPicture('FULF04XX', 2);
    
        $this->assertInstanceOf(ProductPicture::class, $model);
    
        $this->expectException(ClientException::class);
        $resource->findPicture('FULF04XX', 2);
    
        $this->assertEmpty($resource->findPicture('FULF04XX', 2));
    }
}
