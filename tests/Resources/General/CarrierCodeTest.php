<?php
namespace Jtl\Fulfillment\Api\Sdk\Test\Resources\General;

use Jtl\Fulfillment\Api\Sdk\Models\General\CarrierCode;
use Jtl\Fulfillment\Api\Sdk\Resources\General\CarrierCodeResource;
use Jtl\Fulfillment\Api\Sdk\Test\Resources\AbstractResourceTest;

/**
 * Class CarrierCodeTest
 * @package Jtl\Fulfillment\Api\Sdk\Test\Resources\General
 */
class CarrierCodeTest extends AbstractResourceTest
{
    public function testCanQueryAll(): void
    {
        $this->canQueryAll(__DIR__ . '/MockData/carriercode_all.json', CarrierCodeResource::class, CarrierCode::class);
    }
}
