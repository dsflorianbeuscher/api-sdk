<?php
namespace Jtl\Fulfillment\Api\Sdk\Test\Resources\General;

use Izzle\Model\PropertyInfo;
use Jtl\Fulfillment\Api\Sdk\Models\General\User;
use Jtl\Fulfillment\Api\Sdk\Resources\General\UserResource;
use Jtl\Fulfillment\Api\Sdk\Test\Resources\AbstractResourceTest;

/**
 * Class UserTest
 * @package Jtl\Fulfillment\Api\Sdk\Test\Resources\General
 */
class UserTest extends AbstractResourceTest
{
    public function testCanFindOne(): void
    {
        $this->canFindOneBy(
            __DIR__ . '/MockData/user_one.json',
            UserResource::class,
            User::class,
            'current',
            'HJ02',
            new PropertyInfo('userId')
        );
    }
}
