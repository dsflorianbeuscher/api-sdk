<?php
namespace Jtl\Fulfillment\Api\Sdk\Test\Resources\General;

use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use Jtl\Fulfillment\Api\Sdk\Client;
use GuzzleHttp\Client as HttpClient;
use Jtl\Fulfillment\Api\Sdk\Models\General\Product\ProductPicture;
use Jtl\Fulfillment\Api\Sdk\Resources\General\ProductResource;
use Jtl\Fulfillment\Api\Sdk\Test\Resources\AbstractResourceTest;

/**
 * Class ProductTest
 * @package Jtl\Fulfillment\Api\Sdk\Test\Resources\General
 */
class ProductTest extends AbstractResourceTest
{
    public function testCanFindPictureFromUrl(): void
    {
        $mock = new MockHandler([
            new Response(200, [], file_get_contents(__DIR__ . '/MockData/product_picture_one.json')),
            new Response(403),
            new Response(404)
        ]);
    
        $handler = HandlerStack::create($mock);
        $client = new Client(new HttpClient(['handler' => $handler]));
    
        $resource = new ProductResource($client);
    
        $notification = $resource->findPictureFromUrl('https://api.foo.bar/somerandomstring');
    
        $this->assertInstanceOf(ProductPicture::class, $notification);
    
        $this->expectException(ClientException::class);
        $resource->findPictureFromUrl('https://api.foo.bar/somerandomstring');
    
        $this->assertEmpty($resource->findPictureFromUrl('https://api.foo.bar/somerandomstring'));
    }
}
