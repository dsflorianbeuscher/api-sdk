<?php
require_once __DIR__ . '/../bootstrap.php';

/** @var array $config */

use Jtl\Fulfillment\Api\Sdk\Client;
use Jtl\Fulfillment\Api\Sdk\Resources\General\UserResource;

/*
JTL-Fulfillment API SDK
Visit https://ffn.api.jtl-software.com/api-docs/index.html for more infos.
*/

$http = new \GuzzleHttp\Client([
    'base_uri' => $config['base_uri'],
    'headers' => [
        'Authorization' => 'Bearer ' . $config['token'], // Access Token from OAuth2 flow
        'Accept' => 'application/json',
        'Content-Type' => 'application/json'
    ]
]);

// Creating client
try {
    $client = new Client($http, '123'); // User ID as optional second parameter (for caching purpose)
} catch (Throwable $e) {
    var_dump($e->getMessage());
    die();
}

$resource = new UserResource($client);

var_dump($resource->current());
