<?php
require_once __DIR__ . '/../bootstrap.php';

/** @var array $config */

use Jtl\Fulfillment\Api\Sdk\Client;
use Jtl\Fulfillment\Api\Sdk\Resources\Fulfiller\WarehouseResource;
use Jtl\Fulfillment\Api\Sdk\Models\Query;

/*
JTL-Fulfillment API SDK
Visit https://ffn.api.jtl-software.com/api-docs/index.html for more infos.
*/

$http = new \GuzzleHttp\Client([
    'base_uri' => $config['base_uri'],
    'headers' => [
        'Authorization' => 'Bearer ' . $config['token'], // Access Token from OAuth2 flow
        'Accept' => 'application/json',
        'Content-Type' => 'application/json'
    ]
]);

// Creating client
try {
    $client = new Client($http, '123'); // User ID as optional second parameter (for caching purpose)
} catch (Throwable $e) {
    var_dump($e->getMessage());
    die();
}

// Creating new resource
$warehouseResource = new WarehouseResource($client);

// Save Warehouse
/*
try {
    $result = $warehouseResource->save(new Warehouse([
        'name' => 'Daniels Testlager',
        'address' => new Address([
            'salutation' => 'Mr',
            'firstName' => 'Hans',
            'lastName' => 'Meiser',
            'company' => 'JTL-Software-GmbH',
            'street' => 'Krefelder Straße 12',
            'city' => 'Köln',
            'zip' => '41063',
            'country' => 'DE',
            'email' => 'hans.meiser@jtl-software.com',
            'phone' => '1234567',
            'extraLine' => '1234567',
            'extraAddressLine' => 'string',
            'state' => 'NRW',
            'mobile' => '1234567',
            'fax' => '1234567'
        ])
    ]));
    
    var_dump($result);
} catch (Throwable $e) {
    var_dump($e->getMessage());
}
*/

// Creating query
$query = new Query([
    'limit' => 100,
    'offset' => 0
]);

try {
    // Call resource to get all warehouses and filter by query
    $pagination = $warehouseResource->all($query);
    
    echo json_encode($pagination);
} catch (Throwable $e) {
    var_dump($e->getMessage());
}
