<?php
require_once __DIR__ . '/../vendor/autoload.php';

$configPath = __DIR__ . '/config.json';
if (!file_exists($configPath)) {
    die('File config.json does not exists');
}

// Load config file
$config = json_decode(file_get_contents($configPath), true);
